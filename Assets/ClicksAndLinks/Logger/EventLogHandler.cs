﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace ClicksAndLinks.Logger {
    /// <summary>
    /// A singleton implementation of <see cref="ILogHandler"/> that directs log messages to the Windows
    /// Event Log as well as the Unity console.
    ///
    /// Currently it only uses the generic "Application" Event Source because creating new Event Sources requires Admin
    /// privileges.
    /// </summary>
    public class EventLogHandler : ILogHandler {
        private static EventLogHandler _instance;
        private readonly ILogHandler _defaultHandler = UnityEngine.Debug.unityLogger.logHandler;
        private readonly string _eventSourceName;
        private readonly string _messagePrefix;

        private EventLogHandler() {
            _messagePrefix = Application.companyName + ":" + Application.productName;
            _eventSourceName = "Application";

//        _eventSourceName = Application.companyName;
//        if (!System.Diagnostics.EventLog.SourceExists(_eventSourceName)) {
//            System.Diagnostics.EventLog.CreateEventSource(_eventSourceName, "Application");
//        }

            UnityEngine.Debug.unityLogger.logHandler = this;
        }

        /// <summary>
        /// Gets or creates the singleton instance of this class.
        /// </summary>
        public static EventLogHandler Instance {
            get {
                if (_instance == null) {
                    _instance = new EventLogHandler();
                }

                return _instance;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="context"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogFormat(LogType logType, UnityEngine.Object context, string format, params object[] args) {
            EventLogEntryType eventLogEntryType;

            switch (logType) {
                case LogType.Assert:
                case LogType.Error:
                case LogType.Exception:
                    eventLogEntryType = EventLogEntryType.Error;
                    break;

                case LogType.Warning:
                    eventLogEntryType = EventLogEntryType.Warning;
                    break;

                case LogType.Log:
                    eventLogEntryType = EventLogEntryType.Information;
                    break;

                default:
                    eventLogEntryType = EventLogEntryType.Information;
                    break;
            }

            string message = _messagePrefix + ":" + string.Format(format, args);

            System.Diagnostics.EventLog.WriteEntry(_eventSourceName, message, eventLogEntryType, 0, 23);

            _defaultHandler.LogFormat(logType, context, string.Format(format, args));
        }

        public void LogException(Exception exception, UnityEngine.Object context) {
            string message = _messagePrefix + ":exception: " + exception;

            System.Diagnostics.EventLog.WriteEntry(_eventSourceName, message, EventLogEntryType.Error);

            _defaultHandler.LogException(exception, context);
        }
    }
}
