﻿using UnityEngine;

public class FlyCam : MonoBehaviour {
	public float PanSpeed = 2.0f;
	public float rotateSensitivity = 2.0f;
	public float altitudeSensitivity = 1.2f;
	public float runMultiplier = 2.0f;
	public bool lockCursor = true;

	private void Update() {
		Cursor.lockState = lockCursor ? CursorLockMode.Locked : CursorLockMode.None;

		float forward = Input.GetAxis ("Vertical") * PanSpeed * Time.deltaTime;
		float right = Input.GetAxis ("Horizontal") * PanSpeed * Time.deltaTime;
		float altitude = Input.GetAxis ("Mouse ScrollWheel") * altitudeSensitivity;
		bool running = Input.GetButton ("Fire1");

		Vector3 flatForward = Camera.main.transform.forward;
		flatForward.y = 0.0f;
		flatForward.Normalize ();

		Vector3 movementVector = (flatForward * forward) + (Camera.main.transform.right * right) + (Vector3.up * altitude);
		Camera.main.transform.position += movementVector * (running ? runMultiplier : 1.0f);

		float horizontalRotation = Input.GetAxis ("Mouse X") * rotateSensitivity;
		float verticalRotation = Input.GetAxis ("Mouse Y") * rotateSensitivity;
		Camera.main.transform.Rotate (0.0f, horizontalRotation, 0.0f, Space.World);
		Camera.main.transform.Rotate (-verticalRotation, 0.0f, 0.0f, Space.Self);

	}
}
