﻿Shader "ClicksAndLinks/Points" {
	Properties {
		_ParticleSize("Particle Size", float) = 0.01
		_UseColour("Use colour", Int) = 1
	}

	SubShader {
		Tags {"Queue"="Geometry"}
		LOD 100

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"

			struct appdata {
				float4 colour : COLOR;
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 colour : COLOR;
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			float _ParticleSize;
			int _UseColour;

			v2f vert(appdata v) {
				v2f o;

				float4 wpos = mul (unity_ObjectToWorld, v.vertex);

                v.vertex = mul(unity_WorldToObject, wpos);

				o.vertex.xyz = UnityObjectToViewPos(v.vertex.xyz);
				o.vertex.w = 1.0;
				float d = o.vertex.z;
				o.vertex = mul(UNITY_MATRIX_P, o.vertex);

                if (_UseColour == 0) {
                    o.colour.rgb = v.colour.a;
                } else {
                    o.colour.rgb = v.colour.rgb;
                }

 	            o.colour.a = d;
				o.uv = float2(0.0, 0.0);
				return o;
			}

            [maxvertexcount(4)]
            void geom(point v2f input[1], inout TriangleStream<v2f> OutputStream) {
                v2f test = (v2f)0;
                float particle_size = (_ParticleSize * input[0].colour.a);
                test.colour = float4(input[0].colour.rgb, 1.0);
                test.vertex = input[0].vertex;
                test.uv = float2(-0.5, -0.5);
                OutputStream.Append(test);

                test.uv = float2(0.5, -0.5);
                test.vertex = input[0].vertex + float4(particle_size, 0.0, 0.0, 0.0);
                OutputStream.Append(test);

                test.uv = float2(-0.5, 0.5);
                test.vertex = input[0].vertex + float4(0.0, particle_size, 0.0, 0.0);
                OutputStream.Append(test);

                test.uv = float2(0.5, 0.5);
                test.vertex = input[0].vertex + float4(particle_size, particle_size, 0.0, 0.0);
                OutputStream.Append(test);
            }

			fixed4 frag(v2f i) : SV_Target {
			    float r = dot(i.uv, i.uv);
			    if (r > 0.25) {
			        clip(-1);
			    }

                fixed4 col = i.colour;
                return col;
			}
			ENDCG
		}
	}
}
