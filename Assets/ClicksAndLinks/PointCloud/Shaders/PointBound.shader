﻿Shader "Point Bound" {
	Properties {
	    _Color ("Main Color", Color) = (1,1,1,1)
	    _BoundsColor ("Bounds Color", Color) = (1,1,1,1)
	}
	  
	SubShader {
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "LightMode"="ForwardBase"}
	    LOD 100
	    Cull Off
	    ZWrite Off
	    Blend SrcAlpha OneMinusSrcAlpha 
	  
	    CGINCLUDE
	    #include "UnityCG.cginc"

		half4 _Color;
	 	half4 _BoundsColor;
	 
		int _PointBoundVectorLength;
		Vector _PointBoundVectors[100];
		float4x4 _PointBoundMatrices[100];

		struct appdata {
			float4 colour : COLOR;
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

	    struct v2f {
			float4 colour : COLOR;
	        float4 vertex : POSITION;
	        float2 uv : TEXCOORD0;
	    };

		v2f vert(appdata v) {
			v2f o;

			bool inside = false;
	        float4 wpos = mul (unity_ObjectToWorld, v.vertex);
			
			for(int i = 0; i < _PointBoundVectorLength; i ++){
				if(!inside){
					float3 dimensions = _PointBoundVectors[i].xyz;
					float4x4 boundsMatrix = _PointBoundMatrices[i];
					float4 lpos = mul(boundsMatrix, wpos);

					bool x, y, z;
					x = lpos.x >= -dimensions.x/2 && lpos.x <= dimensions.x/2;
					if(x) y = lpos.y >= -dimensions.y/2 && lpos.y <= dimensions.y/2;
					if(y) z = lpos.z >= -dimensions.z/2 && lpos.z <= dimensions.z/2;
					if(x && y && z) inside = true;
				}
			}

        	o.colour = inside ? _BoundsColor : _Color;
			
            v.vertex = mul(unity_WorldToObject, wpos);
			o.vertex = mul(UNITY_MATRIX_MV, v.vertex);
			o.vertex = mul(UNITY_MATRIX_P, o.vertex);

	        o.uv = v.uv;
	        return o;
	    }
	    ENDCG
	  
	    Pass {
	        CGPROGRAM
	        #pragma debug
	        #pragma vertex vert
	        #pragma fragment frag
	        #pragma fragmentoption ARB_precision_hint_fastest     
	        fixed4 frag (v2f i) : COLOR {
	          return i.colour;
	        }
	        ENDCG 
	    }  
	}
}