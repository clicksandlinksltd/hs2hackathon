﻿Shader "ClicksAndLinks/Bounds" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_LineWidth ("Line width", Float) = 0.01
		_GridSize ("Grid size", Float) = 0.25
		_GridColour ("Grid colour", Color) = (1.0, 1.0, 1.0, 1.0)
		_MaxDistance ("Maximum distance", Float) = 1.0
		_AAThresh ("AA threshold", Float) = 1.0
	}
	
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 100
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off
        
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 worldPos : POSITION1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _LineWidth;
			float _GridSize;
			float4 _ViewerPosition;
			fixed4 _GridColour;
			float _MaxDistance;
			float _AAThresh;
			
			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}
			
			fixed4 frag(v2f i) : SV_Target {
			    float modu = i.uv.x % _GridSize;
			    float modv = i.uv.y % _GridSize;
			    
			    float moduderiv = saturate(length(float2(ddx(modu), ddy(modu))) * _AAThresh);
			    float modvderiv = saturate(length(float2(ddx(modv), ddy(modv))) * _AAThresh);
			    float ustep1 = smoothstep(_GridSize - _LineWidth - moduderiv, _GridSize - _LineWidth, modu);
			    float vstep1 = smoothstep(_GridSize - _LineWidth - modvderiv, _GridSize - _LineWidth, modv);
			    float ustep2 = smoothstep(_LineWidth + moduderiv, _LineWidth, modu);
			    float vstep2 = smoothstep(_LineWidth + modvderiv, _LineWidth, modv);
			    float a = max(max(max(ustep1, vstep1), ustep2), vstep2);

				fixed4 col = _GridColour;
				col.a = a * smoothstep(_MaxDistance, _MaxDistance / 2.0, length(i.worldPos.xz - _ViewerPosition.xz));
				return col;
			}
			ENDCG
		}
	}
}
