﻿Shader "ClicksAndLinks/Selection box" {
	Properties {
		_FiducialColour ("Fiducial colour", Color) = (1.0, 1.0, 1.0, 1.0)
		_HighlightColour ("Highlight colour", Color) = (1.0, 1.0, 1.0, 1.0)
		_SecondHighlightColour ("Second highlight colour", Color) = (1.0, 1.0, 1.0, 1.0)
		_FaceColour ("Face colour", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

		Pass {
    		Cull Front
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				fixed4 colour : COLOR;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 colour : COLOR;
				float3 objPos : POSITION1;
			};

			fixed4 _FiducialColour;
			fixed4 _HighlightColour;
			fixed4 _SecondHighlightColour;
			fixed4 _FaceColour;
            float3 _BoxCentre;
            float3 _BoxExtents;

			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.colour = v.colour;
				o.objPos = v.vertex.xyz;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
			    float3 p = abs(i.objPos - _BoxCentre);

			    float fiduciala = step(_BoxExtents.x - 0.15, p.x) * step(_BoxExtents.y - 0.01, p.y) * step(_BoxExtents.z - 0.01, p.z);
			    float fiducialb = step(_BoxExtents.x - 0.01, p.x) * step(_BoxExtents.y - 0.15, p.y) * step(_BoxExtents.z - 0.01, p.z);
			    float fiducialc = step(_BoxExtents.x - 0.01, p.x) * step(_BoxExtents.y - 0.01, p.y) * step(_BoxExtents.z - 0.15, p.z);
				fixed4 corners = saturate(fiduciala + fiducialb + fiducialc) * _FiducialColour;
				
				fixed4 col = lerp(_FaceColour, corners, corners.a);
				col += _HighlightColour * i.colour.r;
				col += _SecondHighlightColour * i.colour.g;
				return col;
			}
			ENDCG
		}

        Pass {
    		Cull Back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				fixed4 colour : COLOR;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 colour : COLOR;
				float3 objPos : POSITION1;
			};

			fixed4 _FiducialColour;
			fixed4 _HighlightColour;
			fixed4 _SecondHighlightColour;
			fixed4 _FaceColour;
            float3 _BoxCentre;
            float3 _BoxExtents;

			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.colour = v.colour;
				o.objPos = v.vertex.xyz;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
			    float3 p = abs(i.objPos - _BoxCentre);

			    float fiduciala = step(_BoxExtents.x - 0.15, p.x) * step(_BoxExtents.y - 0.01, p.y) * step(_BoxExtents.z - 0.01, p.z);
			    float fiducialb = step(_BoxExtents.x - 0.01, p.x) * step(_BoxExtents.y - 0.15, p.y) * step(_BoxExtents.z - 0.01, p.z);
			    float fiducialc = step(_BoxExtents.x - 0.01, p.x) * step(_BoxExtents.y - 0.01, p.y) * step(_BoxExtents.z - 0.15, p.z);
				fixed4 corners = saturate(fiduciala + fiducialb + fiducialc) * _FiducialColour;
				
				fixed4 col = lerp(_FaceColour, corners, corners.a);
				col += _HighlightColour * i.colour.r;
                col += _SecondHighlightColour * i.colour.g;
				return col;
			}
			ENDCG
        }
	}
}
