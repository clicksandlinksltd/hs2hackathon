﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ClicksAndLinks.HPC {
    [CustomEditor(typeof(HPCLoader))]
    public class HPCLoaderEditor : Editor {
        private Editor cachedEditor;

        public void OnEnable() {
            cachedEditor = null;
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            if (EditorApplication.isPlaying) {
                HPCLoader loader = (HPCLoader) target;
                if (GUILayout.Button("Load cloud")) {
                    if (loader.LoadingState == HPCLoader.LoadState.NOT_LOADED) {
                        loader.LoadCloud();
                    }
                }

                if (GUILayout.Button("Load nodes")) {
                    loader.LoadTheTree();
                }

                if (GUILayout.Button("Reset cloud")) {
                    loader.ResetCloud();
                }

                SerializedProperty prop = serializedObject.FindProperty("_objectDatabase");

                ObjectDatabase db = (ObjectDatabase)prop.objectReferenceValue;

                if (db == null) {
                    return;
                }
                
                if (cachedEditor == null) {
                    cachedEditor = CreateEditor(db);
                }

                if (cachedEditor == null) {
                    return;
                }
                
                cachedEditor.DrawDefaultInspector();

                GUILayout.BeginVertical();
                foreach (KeyValuePair<Guid, string> o in db.ObjectNames) {
                    GUILayout.Label(o.Value);
                }
                GUILayout.EndVertical();
            }
        }
    }
}
#endif