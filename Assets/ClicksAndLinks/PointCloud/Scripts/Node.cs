﻿using Newtonsoft.Json;
using UnityEngine;

namespace ClicksAndLinks.HPC {
    public class Node : MonoBehaviour {
        [JsonProperty("id")] public string Id;
        [JsonProperty("bbox")] public float[][] BBox;
        [JsonProperty("transform")] public float[] Transform;
        [JsonProperty("block_offset")] public long BlockOffset;
        [JsonProperty("block_size")] public int BlockSize;
        [JsonProperty("point_count")] public int PointCount;
        [JsonProperty("parent")] public string Parent;
        [JsonProperty("has_normal")] public bool HasNormal = false;
        [JsonProperty("has_colour")] public bool HasColour = true;
        [JsonProperty("has_attribute")] public bool HasAttribute = true;
        [JsonProperty("bsphere")] public float[] BSphere;

        public enum CullReason {
            NONE,
            TOO_MANY_NODES,
            MANUAL,
            PARENT,
            NOT_VISIBLE,
            TOO_DEEP,
            TOO_SMALL
        }

        public Bounds WorldBounds;
        public BoundingSphere SphereBounds;
        public int Depth = -1;
        public bool Culled;
        public Color DebugColour;
        public bool IsLeaf;
        public CullReason TheCullReason;

        public override string ToString() {
            return "<Node: " + name + ">";
        }

        public Rect ScreenRect() {
            Vector3 c = WorldBounds.center;
            Vector3 e = WorldBounds.extents;
            Vector2 p0 = Camera.main.WorldToViewportPoint(new Vector3(c.x - e.x, c.y - e.y, c.z - e.z));
            Vector2 p1 = Camera.main.WorldToViewportPoint(new Vector3(c.x - e.x, c.y - e.y, c.z + e.z));
            Vector2 p2 = Camera.main.WorldToViewportPoint(new Vector3(c.x - e.x, c.y + e.y, c.z - e.z));
            Vector2 p3 = Camera.main.WorldToViewportPoint(new Vector3(c.x - e.x, c.y + e.y, c.z + e.z));
            Vector2 p4 = Camera.main.WorldToViewportPoint(new Vector3(c.x + e.x, c.y - e.y, c.z - e.z));
            Vector2 p5 = Camera.main.WorldToViewportPoint(new Vector3(c.x + e.x, c.y - e.y, c.z + e.z));
            Vector2 p6 = Camera.main.WorldToViewportPoint(new Vector3(c.x + e.x, c.y + e.y, c.z - e.z));
            Vector2 p7 = Camera.main.WorldToViewportPoint(new Vector3(c.x + e.x, c.y + e.y, c.z + e.z));

            Vector2 vmin = p0;
            Vector2 vmax = p0;

            vmin = Vector2.Min(vmin, p1);
            vmin = Vector2.Min(vmin, p2);
            vmin = Vector2.Min(vmin, p3);
            vmin = Vector2.Min(vmin, p4);
            vmin = Vector2.Min(vmin, p5);
            vmin = Vector2.Min(vmin, p6);
            vmin = Vector2.Min(vmin, p7);

            vmax = Vector2.Max(vmax, p1);
            vmax = Vector2.Max(vmax, p2);
            vmax = Vector2.Max(vmax, p3);
            vmax = Vector2.Max(vmax, p4);
            vmax = Vector2.Max(vmax, p5);
            vmax = Vector2.Max(vmax, p6);
            vmax = Vector2.Max(vmax, p7);

            return new Rect(vmin.x, vmin.y, vmax.x - vmin.x, vmax.y - vmin.y);
        }

        public float ScreenSphere() {
            Vector3 c = transform.TransformPoint(SphereBounds.position);
            Vector3 cameraToSphere = c - Camera.main.transform.position;
            float d = (cameraToSphere).magnitude;

            if (d <= SphereBounds.radius) {
                return 1000.0f;
            }

            float weight = 1.0f + Vector3.Dot(Camera.main.transform.forward, cameraToSphere / d);

            float aspectRatio = Screen.height / (float) Screen.width;
            float vfov = Mathf.Deg2Rad * Camera.main.fieldOfView;

            float r = SphereBounds.radius * (1.0f / Mathf.Tan(vfov / 2.0f)) / d;
            float radiusv = r / 2.0f;
            float radiush = radiusv * aspectRatio;

            float area = Mathf.PI * radiusv * radiush;

            return area * weight;
        }

        public Color GetRandomColour() {
            Color[] colors = GetComponent<MeshFilter>().mesh.colors;

            return colors[UnityEngine.Random.Range(0, colors.Length)];
        }

        public Vector4 GetSpherePositionAndRadius() {
            Vector3 c = transform.TransformPoint(SphereBounds.position);
            float radius = WorldBounds.extents.x;

            if (WorldBounds.extents.y > radius)
                radius = WorldBounds.extents.y;

            if (WorldBounds.extents.z > radius)
                radius = WorldBounds.extents.z;

            return new Vector4(c.x, c.y, c.z, radius);
        }

        private void OnDrawGizmos() {
//            if (!IsLeaf) {
//                return;
//            }
            Gizmos.color = DebugColour;
//            Vector3 c = transform.TransformPoint(SphereBounds.position);
//            Gizmos.DrawWireSphere(c, SphereBounds.radius);
//            Vector3 worldBoundsSize = new Vector3(WorldBounds.size.x, 0.01f, WorldBounds.size.z);
//            Gizmos.DrawCube(WorldBounds.center, worldBoundsSize);
            Gizmos.DrawWireCube(WorldBounds.center, WorldBounds.size);
        }
    }
}