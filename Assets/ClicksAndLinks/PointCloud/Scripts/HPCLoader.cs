﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using ClicksAndLinks.Utility;
using GeoJSON.Net;
using Ionic.Zlib;
using UnityEditor;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;

namespace ClicksAndLinks.HPC {
    public class PointCloudException : ApplicationException {
        public PointCloudException() {
        }

        public PointCloudException(string message)
            : base(message) {
        }

        public PointCloudException(string message, Exception inner)
            : base(message, inner) {
        }
    }

    public class HPCLoader : MonoBehaviour {
        private class HpcJsonConverter : CustomCreationConverter<GameObject> {
            private readonly GameObject _nodePrefab;
            private readonly Transform _parentTransform;

            public HpcJsonConverter(GameObject nodePrefab, Transform parentTransform) {
                _nodePrefab = nodePrefab;
                _parentTransform = parentTransform;
            }

            public override GameObject Create(Type objectType) {
                return Instantiate(_nodePrefab);
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                JsonSerializer serializer) {
                JObject jObject = JObject.Load(reader);

                GameObject target = Create(objectType);
                target.transform.parent = _parentTransform;
                Node node = target.AddComponent<Node>();
                serializer.Populate(jObject.CreateReader(), node);
                node.WorldBounds = new Bounds();
                node.WorldBounds.SetMinMax(new Vector3(node.BBox[0][0], node.BBox[0][2], node.BBox[0][1]),
                    new Vector3(node.BBox[1][0], node.BBox[1][2], node.BBox[1][1]));
                node.SphereBounds = new BoundingSphere(new Vector3(node.BSphere[0], node.BSphere[2], node.BSphere[1]),
                    node.BSphere[3]);
                node.DebugColour = Random.ColorHSV(0.0f, 1.0f, 0.8f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f);

                target.name = node.Id;
                return target;
            }
        }

        public enum LoadState {
            NOT_LOADED,
            LOADING,
            LOADED
        }

        public struct Stats {
            public float FPS;
            public int NodeCount;
            public int NodesRendered;
            public int TotalDepth;
            public int DepthRendered;
            public int NodesCulledByFrustum;
            public int NodesCulledBySize;
            public long TotalPoints;
            public long PointsRendered;

            public new string ToString() {
                string s = string.Format("FPS: {0:f2}\n" +
                                         "Nodes rendered: {1}/{2}\n" +
                                         "Node depth rendered: {3}/{4}\n" +
                                         "Nodes culled by frustum: {5}\n" +
                                         "Nodes culled by size: {6}\n" +
                                         "Total points: {7:n0}\n" +
                                         "Points rendered: {8:n0}",
                    FPS, NodesRendered, NodeCount, DepthRendered, TotalDepth, NodesCulledByFrustum, NodesCulledBySize,
                    TotalPoints, PointsRendered);

                return s;
            }
        }

        private struct Version {
            [JsonProperty("major")] public int major;
            [JsonProperty("minor")] public int minor;
        }

        private struct HPCHeader {
            [JsonProperty("version")] public Version version;
            [JsonProperty("srs")] public string SRSProj4;
            [JsonProperty("georef_origin")] public double[] CentreGeoref;
            [JsonProperty("chaperone_bounds")] public FeatureCollection ChaperoneBounds;
            [JsonProperty("nodes")] public List<GameObject> Nodes;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        private struct HPCPoint {
            public float x;
            public float y;
            public float z;
            public byte r;
            public byte g;

            public byte b;

            public short intensity;
        }

        public string PointCloudFileName;
        public GameObject NodePrefab;
        public GameObject BoundsPrefab;
        public float MinArea = 0.04f;
        public int AlwaysVisibleLevel = 2;
        public int NeverVisibleLevel = int.MaxValue;
        public int MaximumNodeCount = int.MaxValue;
        public long MaximumPointCount = long.MaxValue;
        public int NeverLoadLevel = int.MaxValue;
        public bool SizeTest = true;
        public bool FrustumTest = true;
        public bool AutoLoad;

        public delegate void Progress(float percent);

        public event Progress OnProgress;

        public delegate void Loaded(int nodeCount, long pointCount, long loadTimeMs);

        public event Loaded OnLoaded;

        public delegate void LoadStart();

        public event LoadStart OnLoadStart;

        public delegate void GetStats(Stats stats);

        public event GetStats OnGetStats;

        private const float FpsUpdateRate = 1.0f;

        private List<Node> _nodes;
        private Plane[] _frustumPlanes;
        private long _totalPointCount;
        private float _fpsTime;
        private int _fpsFrameCount;
        private float _fpsLastRate;
        private int _maxDepth;
        private Stats _stats;
        private LoadState _loadState;
        private int _nodeCount;
        private List<Renderer> _boundsRenderers;
        [SerializeField]
        private ObjectDatabase _objectDatabase;

        public LoadState LoadingState {
            get { return _loadState; }
        }

        private void Start() {
            _loadState = LoadState.NOT_LOADED;
            if (AutoLoad) {
                LoadCloud();
            }
        }

        public void LoadCloud() {
            if (_loadState != LoadState.NOT_LOADED) {
                throw new PointCloudException("The point cloud has been loaded already.");
            }

            try {
                _loadState = LoadState.LOADING;
                ResetCloud();
                string hpcFilePath = FindFile.Instance.Find(Path.ChangeExtension(PointCloudFileName, "hpc"));
                string jsonFilePath = FindFile.Instance.Find(Path.ChangeExtension(PointCloudFileName, "json"));
                string message = "Found HPC file in \"" + hpcFilePath + "\"";
                Logger.EventLog.Info(Application.productName, message);

                StartCoroutine(LoadIt(hpcFilePath, jsonFilePath));
            } catch (FileNotFoundException e) {
                _loadState = LoadState.NOT_LOADED;
                string message = e.Message + " \"" + e.FileName + "\"" + " in any of these paths:\n" +
                                 FindFile.Instance;
                Logger.EventLog.Error(Application.productName, message);
                throw new PointCloudException(message, e);
            }
        }

        public void ResetCloud() {
            if (transform.childCount > 0) {
                Destroy(transform.GetChild(0).gameObject);
            }

            if (_nodes != null) {
                _nodes.Clear();
            } else {
                _nodes = new List<Node>();
            }

            _nodeCount = 0;
            _loadState = LoadState.NOT_LOADED;
            _stats = new Stats();
            _boundsRenderers = new List<Renderer>();
            GC.Collect();
        }

        public void LoadTheTree() {
            ResetCloud();

            try {
                _loadState = LoadState.LOADING;
                string jsonFilePath = FindFile.Instance.Find(Path.ChangeExtension(PointCloudFileName, "json"));
                string message = "Found JSON file in \"" + jsonFilePath + "\"";
                Logger.EventLog.Info(Application.productName, message);

                StartCoroutine(LoadTree(jsonFilePath));
            } catch (FileNotFoundException e) {
                _loadState = LoadState.NOT_LOADED;
                string message = e.Message + " \"" + e.FileName + "\"" + " in any of these paths:\n" +
                                 FindFile.Instance;
                Logger.EventLog.Error(Application.productName, message);
                throw new PointCloudException(message, e);
            }
        }

        private IEnumerator LoadTree(string jsonFileName) {
            Stopwatch watch = Stopwatch.StartNew();
            long start = watch.ElapsedMilliseconds;
            float progress = 0.0f;

            using (StreamReader file = File.OpenText(jsonFileName)) {
                HPCHeader hpc = JsonConvert.DeserializeObject<HPCHeader>(file.ReadToEnd(),
                    new HpcJsonConverter(NodePrefab, transform));

                if (!(hpc.version.major == 1 && hpc.version.minor == 1)) {
                    Logger.EventLog.Error(Application.productName,
                        "Wrong HPC version. Expected 1.1, got " + hpc.version.major + "." + hpc.version.minor);
                    yield break;
                }

                _nodeCount = hpc.Nodes.Count;

                foreach (GameObject go in hpc.Nodes) {
                    // Update at about 60 FPS.
                    if (watch.ElapsedMilliseconds - start > 16) {
                        if (OnProgress != null) {
                            OnProgress(progress / _nodeCount);
                        }

                        yield return null;
                        start = watch.ElapsedMilliseconds;
                    }

                    Node node = go.GetComponent<Node>();
                    node.IsLeaf = true;
                    go.transform.position = new Vector3(node.Transform[3], node.Transform[11], node.Transform[7]);
                    go.transform.localRotation = Quaternion.identity;
                    go.transform.localScale = ExtractScale(node.Transform);

                    _totalPointCount += node.PointCount;

                    if (node.Parent != null) {
                        Node parent = _nodes.FirstOrDefault(n => n.Id == node.Parent);
                        if (parent != null) {
                            parent.IsLeaf = false;
                            go.transform.SetParent(parent.transform, true);
                            node.Depth = parent.GetComponent<Node>().Depth + 1;
                            _maxDepth = Math.Max(_maxDepth, node.Depth);
                        } else {
                            Debug.LogWarning(string.Format("Node {0} cannot find its parent {1}", node.Id,
                                node.Parent));
                            go.transform.SetParent(transform, true);
                            node.Depth = 0;
                        }
                    } else {
                        go.transform.SetParent(transform, true);
                        node.Depth = 0;
                    }

                    progress += 0.5f;

                    _nodes.Add(node);
                }

                GenerateChaperoneBounds(hpc);
            }

            if (_objectDatabase == null) {
                _objectDatabase = ScriptableObject.CreateInstance<ObjectDatabase>();
            }

            if (_objectDatabase != null) {
                _objectDatabase.Load(Path.GetDirectoryName(jsonFileName) + "/data.json");
                _objectDatabase.SelectObject(new Guid("c7b7adee-434f-438b-aeed-32bb31cbbb65"));
            } else {
                throw new NullReferenceException("Unable to create an ObjectDatabase");
            }

            _loadState = LoadState.LOADED;
        }

        private void GenerateChaperoneBounds(HPCHeader header) {
            if (header.ChaperoneBounds == null) {
                return;
            }

            float lowestPoint = _nodes[0].BBox[0][2];
            float highestPoint = _nodes[0].BBox[1][2];
            
//            foreach (Feature feature in header.ChaperoneBounds.Features) {
            for (int i = 0; i < header.ChaperoneBounds.Features.Count; ++i) {
                Feature feature = header.ChaperoneBounds.Features[i];
                // TODO: Handle multiploygons.
                if (feature.Geometry.Type != GeoJSONObjectType.Polygon) {
                    continue;
                }

                GameObject boundsGo = Instantiate(BoundsPrefab);
                _boundsRenderers.Add(boundsGo.GetComponent<Renderer>());
                boundsGo.transform.parent = transform;
                Polygon polygon = (Polygon) feature.Geometry;
                boundsGo.name = "chaperone_bounds_" + feature.Properties["id"];

                List<Vector3> points = new List<Vector3>();
                List<Vector2> uvs = new List<Vector2>();
                List<int> indices = new List<int>();

                int index = 0;

//                foreach (LineString lineString in polygon.Coordinates) {
                for (int l = 0; l < polygon.Coordinates.Count; ++l) {
                    LineString lineString = polygon.Coordinates[l];
                    float length = 0.0f;
                    Vector3 prevPos = Vector3.zero;
                    bool first = true;

//                    foreach (IPosition position in lineString.Coordinates) {
                    for (int p = 0; p < lineString.Coordinates.Count; ++p) {
                        IPosition position = lineString.Coordinates[p];
                        // TODO: Handle coordinate transformation.
                        Vector3 coord = new Vector3((float) (position.Longitude - header.CentreGeoref[0]), lowestPoint,
                            (float) (position.Latitude - header.CentreGeoref[1]));

                        if (!first) {
                            length += (coord - prevPos).magnitude;
                            int idx = index + p * 2;
                            indices.Add(idx - 2);
                            indices.Add(idx - 1);
                            indices.Add(idx);
                            indices.Add(idx);
                            indices.Add(idx - 1);
                            indices.Add(idx + 1);
                        }

                        first = false;

                        prevPos = coord;
                        points.Add(coord);
                        points.Add(coord + Vector3.up * (highestPoint - lowestPoint));
                        uvs.Add(new Vector2(length, 0.0f));
                        uvs.Add(new Vector2(length, highestPoint - lowestPoint));
                    }

                    index = points.Count;
                }

                if (points.Count > 0) {
                    MeshFilter meshFilter = boundsGo.GetComponent<MeshFilter>();

                    Mesh mesh = new Mesh();
                    mesh.vertices = points.ToArray();
                    mesh.uv = uvs.ToArray();

                    mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0);
                    meshFilter.mesh = mesh;
                }
            }
        }

        private IEnumerator LoadIt(string hpcFileName, string jsonFileName) {
            Stopwatch watch = Stopwatch.StartNew();

            if (OnLoadStart != null) {
                OnLoadStart();
            }

            if (OnProgress != null) {
                OnProgress(0.0f);
            }

            long start = watch.ElapsedMilliseconds;
            float progress = 0.0f;

            yield return LoadTree(jsonFileName);

            using (FileStream data = new FileStream(hpcFileName, FileMode.Open)) {
                foreach (Node node in _nodes) {
                    if (watch.ElapsedMilliseconds - start > 16) {
                        if (OnProgress != null) {
                            OnProgress(progress / _nodeCount + 0.5f);
                        }

                        yield return null;
                        start = watch.ElapsedMilliseconds;
                    }

                    progress += 0.5f;

                    if (node.Depth >= NeverLoadLevel) {
                        continue;
                    }

                    PopulateMesh(node.gameObject, data);
                }
            }

            watch.Stop();
            Debug.Log(string.Format("Loaded {0} nodes of {1} points in {2} ms", _nodeCount, _totalPointCount,
                watch.ElapsedMilliseconds));

            if (OnProgress != null) {
                OnProgress(1.0f);
            }

            if (OnLoaded != null) {
                OnLoaded(_nodeCount, _totalPointCount, watch.ElapsedMilliseconds);
            }

            _loadState = LoadState.LOADED;

            yield return null;
        }

        private static Vector3 ExtractScale(float[] m) {
            Matrix4x4 matrix = new Matrix4x4 {
                m00 = m[0],
                m01 = m[1],
                m02 = m[2],
                m03 = m[3],
                m10 = m[4],
                m11 = m[5],
                m12 = m[6],
                m13 = m[7],
                m20 = m[8],
                m21 = m[9],
                m22 = m[10],
                m23 = m[11],
                m30 = m[12],
                m31 = m[13],
                m32 = m[14],
                m33 = m[15]
            };

            Vector3 scale = new Vector3(
                matrix.GetColumn(0).magnitude,
                matrix.GetColumn(1).magnitude,
                matrix.GetColumn(2).magnitude
            );

            if (matrix.determinant < 0.0f) {
//        if (Vector3.Cross (matrix.GetColumn (0), matrix.GetColumn (1)).normalized != (Vector3)matrix.GetColumn (2).normalized) {
                scale.x *= -1;
            }

            return scale;
        }

        private void Update() {
            if (_boundsRenderers != null) {
                foreach (Renderer boundsRenderer in _boundsRenderers) {
                    Vector3 viewerPos = Camera.main.transform.position;
                    boundsRenderer.material.SetVector("_ViewerPosition",
                        new Vector4(viewerPos.x, viewerPos.y, viewerPos.z, 1.0f));
                }
            }
            
            if (_nodes == null || _nodes.Count == 0 || !EditorApplication.isPlaying) {
                return;
            }

            // TODO: Implement: https://cesium.com/blog/2015/08/04/fast-hierarchical-culling/
            _frustumPlanes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

            int activeNodeCount = 0;
            long renderedPointCount = 0;
            int culledByFrustum = 0;
            int culledBySize = 0;
            int renderedTreeDepth = 0;

            foreach (Node node in _nodes) {
                // Disable everything we don't have enough time for.
                if (activeNodeCount > MaximumNodeCount || renderedPointCount > MaximumPointCount) {
                    node.TheCullReason = Node.CullReason.TOO_MANY_NODES;
                    node.gameObject.SetActive(false);
                    continue;
                }

                // Manually overridden culling.
                if (node.Culled) {
                    node.TheCullReason = Node.CullReason.MANUAL;
                    node.gameObject.SetActive(false);
                    continue;
                }

                // Disable a node who's parent is disabled.
                if (!node.transform.parent.gameObject.activeSelf) {
                    node.TheCullReason = Node.CullReason.PARENT;
                    node.gameObject.SetActive(false);
                    continue;
                }

                // Disable everything we can't see.
                if (FrustumTest && !GeometryUtility.TestPlanesAABB(_frustumPlanes, node.WorldBounds)) {
                    node.TheCullReason = Node.CullReason.NOT_VISIBLE;
                    node.gameObject.SetActive(false);
                    ++culledByFrustum;
                    continue;
                }

                // Otherwise, enable nodes above the manually-set threshold depth.
                if (node.Depth <= AlwaysVisibleLevel) {
                    node.TheCullReason = Node.CullReason.NONE;
                    node.gameObject.SetActive(true);
                    ++activeNodeCount;
                    renderedPointCount += node.PointCount;
                    renderedTreeDepth = Math.Max(renderedTreeDepth, node.Depth);
                    continue;
                }

                // Disable nodes below the manually-set threshold depth.
                // If we're loading, only render the always visible nodes to help maintain
                // framerate.
                if (node.Depth >= NeverVisibleLevel ||
                    (_loadState == LoadState.LOADING && node.Depth > AlwaysVisibleLevel)) {
                    node.TheCullReason = Node.CullReason.TOO_DEEP;
                    node.gameObject.SetActive(false);
                    continue;
                }

                // Disable anything smaller than some threshold.
                float area = node.ScreenSphere();
                if (SizeTest && area < MinArea) {
                    node.TheCullReason = Node.CullReason.TOO_SMALL;
                    node.gameObject.SetActive(false);
                    ++culledBySize;
                    continue;
                }

                node.gameObject.SetActive(true);
                ++activeNodeCount;
                renderedPointCount += node.PointCount;
                renderedTreeDepth = Math.Max(renderedTreeDepth, node.Depth);
            }

            if (OnGetStats != null) {
                if (_fpsTime < FpsUpdateRate) {
                    _fpsTime += Time.deltaTime;
                    ++_fpsFrameCount;
                } else {
                    _fpsLastRate = _fpsFrameCount / _fpsTime;
                    _fpsTime = 0.0f;
                    _fpsFrameCount = 0;
                }
                _stats.FPS = _fpsLastRate;
                _stats.NodeCount = _nodes.Count;
                _stats.NodesRendered = activeNodeCount;
                _stats.TotalDepth = _maxDepth;
                _stats.DepthRendered = renderedTreeDepth;
                _stats.NodesCulledByFrustum = culledByFrustum;
                _stats.NodesCulledBySize = culledBySize;
                _stats.TotalPoints = _totalPointCount;
                _stats.PointsRendered = renderedPointCount;

                OnGetStats(_stats);
            }
        }

        private static void PopulateMesh(GameObject go, Stream data) {
            Node node = go.GetComponent<Node>();
            Mesh mesh = new Mesh();
            Vector3[] points = new Vector3[node.PointCount];
            int[] indices = new int[node.PointCount];
            Vector2[] uvs = new Vector2[node.PointCount];
            Color[] colours = new Color[node.PointCount];

            data.Seek(node.BlockOffset, SeekOrigin.Begin);

            byte[] blockBuffer = new byte[16384 * 18];
            MemoryStream blockBufferStream = new MemoryStream(blockBuffer, true);
            ZlibStream zls = new ZlibStream(blockBufferStream, CompressionMode.Decompress, false);
            byte[] buf2 = new byte[node.BlockSize];
            int len = 0;

            while (len < node.BlockSize) {
                int bytesRead = data.Read(buf2, 0, buf2.Length);
                zls.Write(buf2, 0, bytesRead);
                len += bytesRead;
            }
            zls.Flush();
            zls.Close();

            for (int i = 0; i < node.PointCount; ++i) {
                HPCPoint point;
                
                unsafe {
                    fixed (byte* packet = &blockBuffer[i * 18]) {
                        point = *(HPCPoint*) packet;
                    }
                }

                colours[i] = new Color32(point.r, point.g, point.b, (byte) (point.intensity >> 8));

                points[i] = new Vector3(point.x, point.z, point.y);
                indices[i] = i;
                uvs[i] = new Vector2(point.intensity, 0.0f);
            }

            mesh.vertices = points;
            mesh.colors = colours;
            mesh.uv = uvs;

            mesh.SetIndices(indices, MeshTopology.Points, 0);
            go.GetComponent<MeshFilter>().mesh = mesh;
        }
    }
}