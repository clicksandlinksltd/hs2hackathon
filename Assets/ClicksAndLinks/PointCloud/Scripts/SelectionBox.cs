﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

namespace ClicksAndLinks.HPC {
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(BoxCollider))]
    public class SelectionBox : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler,
        IPointerExitHandler {
        private enum Face {
            PositiveX,
            NegativeX,
            PositiveY,
            NegativeY,
            PositiveZ,
            NegativeZ,

            Unknown
        }

        private enum EditState {
            NotEditing,
            Dragging,
            Rotating,
            Moving
        }

        public Vector3 Dimensions { get {return _boundingBox.size;}}

        public Vector3 Orientation { get {return transform.localRotation.eulerAngles;}}

        public Vector3 Position { get {return transform.localPosition;}}

        public float Volume {
            get { return Dimensions.x * Dimensions.y * Dimensions.z; }
        }

        public Bounds AABB {get { return _AABB; }}

        private const float MINIMUM_SIZE = 0.1f;

        private MeshFilter _meshFilter;
        private MeshRenderer _renderer;
        private BoxCollider _collider;
        private Bounds _boundingBox;
        private Bounds _AABB;
        private Face _currentFace = Face.Unknown;
        private Face _axisFace = Face.Unknown;
        private EditState _editState;
        private Vector3 _mouseDownPos;
        private Vector3 _mouseDownFaceNormal;
        private Vector3 _mouseDownNormal;
        private Quaternion _mouseDownRotation;
        private float _mouseDownDimension;
        private Vector3 _rotateAxis;
        private Plane _movementPlane;

        public void Init(Vector3 position, Vector3 size, Quaternion orientation) {
            transform.localPosition = position;
            _boundingBox.size = size;
            transform.localRotation = orientation;
            _meshFilter.mesh.vertices = GenerateVertices().ToArray();
            UpdateAABB();
        }

        private void Awake() {
            _editState = EditState.NotEditing;
            _boundingBox = new Bounds(transform.position, Vector3.one);
            _collider = gameObject.GetComponent<BoxCollider>();
            _collider.center = _boundingBox.center;
            _collider.size = _boundingBox.size;

            _meshFilter = gameObject.GetComponent<MeshFilter>();
            _renderer = gameObject.GetComponent<MeshRenderer>();

            _renderer.lightProbeUsage = LightProbeUsage.Off;
            _renderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
            _renderer.shadowCastingMode = ShadowCastingMode.Off;
            _renderer.receiveShadows = false;
            _renderer.sharedMaterial = (Material) Resources.Load("SelectionBox", typeof(Material));

            BuildBox();

            gameObject.layer = LayerMask.NameToLayer("Selection");
        }

        private void Update() {
            Color[] colours = _meshFilter.sharedMesh.colors;

            for (int i = 0; i < colours.Length; ++i) {
                colours[i] = Color.black;
            }

            if (_currentFace != Face.Unknown) {
                int v = (int) _currentFace * 4;

                // We just use the red channel as a way of marking a vertex as highlighted.
                colours[v + 0] = Color.red;
                colours[v + 1] = Color.red;
                colours[v + 2] = Color.red;
                colours[v + 3] = Color.red;
            }

            if (_axisFace != Face.Unknown) {
                int v = (int) _axisFace * 4;

                // We just use the green channel as a way of marking a vertex as highlighted with the second controller.
                colours[v + 0] = Color.green;
                colours[v + 1] = Color.green;
                colours[v + 2] = Color.green;
                colours[v + 3] = Color.green;
            }

            _meshFilter.sharedMesh.colors = colours;

            if (_currentFace == Face.Unknown) {
                return;
            }

            // TODO: Use the EventSystem ray instead.
            Ray pointerRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            switch (_editState) {
                case EditState.NotEditing:
                    SelectFace();
                    break;

                case EditState.Dragging:
                    HandleDrag(pointerRay);
                    break;

                case EditState.Rotating:
                    HandleRotate(pointerRay);
                    break;

                case EditState.Moving:
                    HandleTranslate(pointerRay);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void InitDrag() {
            switch (_currentFace) {
                case Face.PositiveX:
                    _mouseDownDimension = _boundingBox.max.x;
                    break;

                case Face.NegativeX:
                    _mouseDownDimension = _boundingBox.min.x;
                    break;

                case Face.PositiveY:
                    _mouseDownDimension = _boundingBox.max.y;
                    break;

                case Face.NegativeY:
                    _mouseDownDimension = _boundingBox.min.y;
                    break;

                case Face.PositiveZ:
                    _mouseDownDimension = _boundingBox.max.z;
                    break;

                case Face.NegativeZ:
                    _mouseDownDimension = _boundingBox.min.z;
                    break;

                case Face.Unknown:
                    throw new ArgumentOutOfRangeException();

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void InitRotate() {
            _mouseDownNormal = (_mouseDownPos - _boundingBox.center).normalized;
            _mouseDownRotation = transform.rotation;
            
            // TODO: Use the second controller ray to determine the axis face.
            _axisFace = Face.NegativeZ;
            _rotateAxis = Vector3.forward;
        }

        private void InitTranslate() {
            _movementPlane = new Plane(_mouseDownFaceNormal, _mouseDownPos);
        }

        private void HandleDrag(Ray pointerRay) {
            Ray faceRay = new Ray(_mouseDownPos, _mouseDownFaceNormal);

            float facePoint;
            float pointerPoint;

            if (!ClosestPointsOnTwoLines(out facePoint, out pointerPoint, faceRay, pointerRay)) {
                return;
            }

            Vector3 bmax = _boundingBox.max;
            Vector3 bmin = _boundingBox.min;

            facePoint /= 2.0f;

            switch (_currentFace) {
                case Face.PositiveX:
                    bmax.x = _mouseDownDimension + facePoint;
                    break;

                case Face.NegativeX:
                    bmin.x = _mouseDownDimension - facePoint;
                    break;

                case Face.PositiveY:
                    bmax.y = _mouseDownDimension + facePoint;
                    break;

                case Face.NegativeY:
                    bmin.y = _mouseDownDimension - facePoint;
                    break;

                case Face.PositiveZ:
                    bmax.z = _mouseDownDimension + facePoint;
                    break;

                case Face.NegativeZ:
                    bmin.z = _mouseDownDimension - facePoint;
                    break;

                case Face.Unknown:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (bmax.x - bmin.x < MINIMUM_SIZE || bmax.y - bmin.y < MINIMUM_SIZE || bmax.z - bmin.z < MINIMUM_SIZE) {
                return;
            }

            Vector3 diff = (bmax + bmin) / 2.0f - _boundingBox.center;

            _boundingBox.max = bmax;
            _boundingBox.min = bmin;

            transform.Translate(diff, Space.Self);
            _boundingBox.center -= diff;

            _meshFilter.mesh.vertices = GenerateVertices().ToArray();
            UpdateAABB();
        }

        private void HandleRotate(Ray pointerRay) {
            Ray faceRay = new Ray(_mouseDownPos, _mouseDownFaceNormal);

            float facePoint;
            float pointerPoint;

            if (!ClosestPointsOnTwoLines(out facePoint, out pointerPoint, faceRay, pointerRay)) {
                return;
            }

            Vector3 requestedVec = pointerRay.direction * pointerPoint + pointerRay.origin - _boundingBox.center;

            requestedVec = Vector3.ProjectOnPlane(requestedVec, _rotateAxis).normalized;
            Vector3 mdVec = Vector3.ProjectOnPlane(_mouseDownNormal, _rotateAxis).normalized;

            Quaternion q = Quaternion.FromToRotation(mdVec, requestedVec);

            transform.rotation = _mouseDownRotation * q;
            UpdateAABB();
        }

        private void HandleTranslate(Ray pointerRay) {
            float distance;
            _movementPlane.Raycast(pointerRay, out distance);

            if (distance == 0.0f) {
                return;
            }

            Vector3 mousePos = pointerRay.GetPoint(distance);
            transform.Translate(mousePos - _mouseDownPos, Space.World);
            _mouseDownPos = mousePos;
            UpdateAABB();
        }

        private static bool ClosestPointsOnTwoLines(out float t1, out float t2, Ray line1, Ray line2) {
            float a = Vector3.Dot(line1.direction, line1.direction);
            float b = Vector3.Dot(line1.direction, line2.direction);
            float e = Vector3.Dot(line2.direction, line2.direction);

            float d = a * e - b * b;

            t1 = 0.0f;
            t2 = 0.0f;

            // Lines are parallel.
            if (d <= Mathf.Epsilon) {
                return false;
            }

            Vector3 r = line1.origin - line2.origin;
            float c = Vector3.Dot(line1.direction, r);
            float f = Vector3.Dot(line2.direction, r);

            t1 = (b * f - c * e) / d;
            t2 = (a * f - c * b) / d;

            return true;
        }

        public void OnPointerEnter(PointerEventData eventData) {
            if (_editState != EditState.NotEditing) {
                return;
            }

            SelectFace();
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (_editState != EditState.NotEditing) {
                return;
            }

            _currentFace = Face.Unknown;
            _axisFace = Face.Unknown;
        }

        public void OnPointerDown(PointerEventData pointerEventData) {
            if (_currentFace == Face.Unknown) {
                return;
            }

            _mouseDownPos = pointerEventData.pointerPressRaycast.worldPosition;
            _mouseDownFaceNormal = pointerEventData.pointerPressRaycast.worldNormal;

            switch (pointerEventData.button) {
                case PointerEventData.InputButton.Left:
                    _editState = EditState.Dragging;
                    InitDrag();
                    break;
                case PointerEventData.InputButton.Right:
                    _editState = EditState.Rotating;
                    InitRotate();
                    break;
                case PointerEventData.InputButton.Middle:
                    _editState = EditState.Moving;
                    InitTranslate();
                    break;
                default:
                    _editState = EditState.NotEditing;
                    break;
            }
        }

        public void OnPointerUp(PointerEventData eventData) {
            _editState = EditState.NotEditing;
            _axisFace = Face.Unknown;
        }

        private void SelectFace() {
            _currentFace = Face.Unknown;

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
                _currentFace = NormalToFace(hit.normal);

            }
        }

        public bool IntersectsBounds(Bounds bounds) {
            if (!_AABB.Intersects(bounds)) {
                return false;
            }
            
            Vector3 p = transform.worldToLocalMatrix.MultiplyPoint(bounds.min);

            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.min.x, bounds.min.y, bounds.max.z));
            
            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.min.x, bounds.max.y, bounds.min.z));
            
            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.min.x, bounds.max.y, bounds.max.z));
            
            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.max.x, bounds.min.y, bounds.min.z));
            
            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.max.x, bounds.min.y, bounds.max.z));
            
            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.max.x, bounds.max.y, bounds.min.z));
            
            if (_boundingBox.Contains(p)) {
                return true;
            }
            
            p = transform.worldToLocalMatrix.MultiplyPoint(new Vector3(bounds.max.x, bounds.max.y, bounds.max.z));
            
            return _boundingBox.Contains(p);
        }

        private Face NormalToFace(Vector3 normal) {
            Vector3 norm = transform.worldToLocalMatrix.MultiplyVector(normal);

            if (norm.x > 0.9f) {
                return Face.PositiveX;
            }

            if (norm.x < -0.9f) {
                return Face.NegativeX;
            }

            if (norm.y > 0.9f) {
                return Face.PositiveY;
            }

            if (norm.y < -0.9f) {
                return Face.NegativeY;
            }

            if (norm.z > 0.9f) {
                return Face.PositiveZ;
            }

            if (norm.z < -0.9f) {
                return Face.NegativeZ;
            }

            return Face.Unknown;
        }

        private void UpdateAABB() {
            if (_meshFilter == null || _meshFilter.mesh == null || _meshFilter.mesh.vertices.Length < 8) {
                return;
            }
            
            Vector3 p = transform.TransformPoint(_meshFilter.mesh.vertices[0]);
            float minX = p.x;
            float minY = p.y;
            float minZ = p.z;
            float maxX = p.x;
            float maxY = p.y;
            float maxZ = p.z;

            for (int i = 1; i < 8; ++i) {
                p = transform.TransformPoint(_meshFilter.mesh.vertices[i]);

                minX = Mathf.Min(minX, p.x);
                minY = Mathf.Min(minY, p.y);
                minZ = Mathf.Min(minZ, p.z);
                maxX = Mathf.Max(maxX, p.x);
                maxY = Mathf.Max(maxY, p.y);
                maxZ = Mathf.Max(maxZ, p.z);
            }
            
            Vector3 xformSize = new Vector3(maxX - minX, maxY - minY, maxZ - minZ);
            _AABB = new Bounds(transform.position, xformSize);
        }
        
        private List<Vector3> GenerateVertices() {
            List<Vector3> vertices = new List<Vector3>(24) {
                // Positive X
                new Vector3(_boundingBox.max.x, _boundingBox.min.y, _boundingBox.min.z),
                new Vector3(_boundingBox.max.x, _boundingBox.min.y, _boundingBox.max.z),
                new Vector3(_boundingBox.max.x, _boundingBox.max.y, _boundingBox.min.z),
                new Vector3(_boundingBox.max.x, _boundingBox.max.y, _boundingBox.max.z),
                // Negative X
                new Vector3(_boundingBox.min.x, _boundingBox.min.y, _boundingBox.min.z),
                new Vector3(_boundingBox.min.x, _boundingBox.min.y, _boundingBox.max.z),
                new Vector3(_boundingBox.min.x, _boundingBox.max.y, _boundingBox.min.z),
                new Vector3(_boundingBox.min.x, _boundingBox.max.y, _boundingBox.max.z),
                // Positive Y
                new Vector3(_boundingBox.min.x, _boundingBox.max.y, _boundingBox.min.z),
                new Vector3(_boundingBox.min.x, _boundingBox.max.y, _boundingBox.max.z),
                new Vector3(_boundingBox.max.x, _boundingBox.max.y, _boundingBox.min.z),
                new Vector3(_boundingBox.max.x, _boundingBox.max.y, _boundingBox.max.z),
                // Negative Y
                new Vector3(_boundingBox.min.x, _boundingBox.min.y, _boundingBox.min.z),
                new Vector3(_boundingBox.min.x, _boundingBox.min.y, _boundingBox.max.z),
                new Vector3(_boundingBox.max.x, _boundingBox.min.y, _boundingBox.min.z),
                new Vector3(_boundingBox.max.x, _boundingBox.min.y, _boundingBox.max.z),
                // Positive Z
                new Vector3(_boundingBox.min.x, _boundingBox.min.y, _boundingBox.max.z),
                new Vector3(_boundingBox.min.x, _boundingBox.max.y, _boundingBox.max.z),
                new Vector3(_boundingBox.max.x, _boundingBox.min.y, _boundingBox.max.z),
                new Vector3(_boundingBox.max.x, _boundingBox.max.y, _boundingBox.max.z),
                // Negative Z
                new Vector3(_boundingBox.min.x, _boundingBox.min.y, _boundingBox.min.z),
                new Vector3(_boundingBox.min.x, _boundingBox.max.y, _boundingBox.min.z),
                new Vector3(_boundingBox.max.x, _boundingBox.min.y, _boundingBox.min.z),
                new Vector3(_boundingBox.max.x, _boundingBox.max.y, _boundingBox.min.z)
            };
            
            _collider.center = _boundingBox.center;
            _collider.size = _boundingBox.size;

            _renderer.sharedMaterial.SetVector("_BoxCentre", (_boundingBox.max + _boundingBox.min) * 0.5f);
            _renderer.sharedMaterial.SetVector("_BoxExtents", _boundingBox.extents);

            return vertices;
        }

        private void BuildBox() {
            List<int> indices = new List<int>();
            List<Color> colours = new List<Color>();

            // Positive X
            colours.AddRange(new[] {Color.black, Color.black, Color.black, Color.black});
            indices.AddRange(new[] {0, 2, 1, 1, 2, 3});

            // Negative X
            colours.AddRange(new[] {Color.black, Color.black, Color.black, Color.black});
            indices.AddRange(new[] {4, 5, 6, 5, 7, 6});

            // Positive Y
            colours.AddRange(new[] {Color.black, Color.black, Color.black, Color.black});
            indices.AddRange(new[] {8, 9, 10, 10, 9, 11});

            // Negative Y
            colours.AddRange(new[] {Color.black, Color.black, Color.black, Color.black});
            indices.AddRange(new[] {12, 14, 13, 15, 13, 14});

            // Positive Z
            colours.AddRange(new[] {Color.black, Color.black, Color.black, Color.black});
            indices.AddRange(new[] {16, 18, 17, 17, 18, 19});

            // Negative Z
            colours.AddRange(new[] {Color.black, Color.black, Color.black, Color.black});
            indices.AddRange(new[] {20, 21, 22, 21, 23, 22});

            Mesh mesh = new Mesh {
                name = "Faces",
                vertices = GenerateVertices().ToArray(),
                colors = colours.ToArray()
            };

            mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0, true);

            _meshFilter.mesh = mesh;
        }
    }
}