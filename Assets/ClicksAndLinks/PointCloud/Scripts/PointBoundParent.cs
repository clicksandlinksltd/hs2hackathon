﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System.Linq;
using Newtonsoft.Json;
using UnityEditor;

namespace ClicksAndLinks.HPC {
    public class SelectionException : ApplicationException {
        public SelectionException() {
        }

        public SelectionException(string message)
            : base(message) {
        }

        public SelectionException(string message, Exception inner)
            : base(message, inner) {
        }
    }

    public class PointBoundParent : MonoBehaviour {
        private string Name {
            get { return _objectName; }
        }

        private Vector3 worldPosition {
            get { return _AABB.center; }
        }

        private Vector3 worldDimensions {
            get { return _AABB.size; }
        }

        public ReadOnlyCollection<SelectionBox> Children {
            get { return _children.AsReadOnly(); }
        }

        public GameObject childBoundsPrefab;

        public bool debug = true;

        private List<SelectionBox> _children = new List<SelectionBox>();
        private Bounds _AABB;
        private string _objectName;

        public void Init(ObjectDatabase.PointObject o) {
            _children.Clear();
            _objectName = o.Name;

            foreach (ObjectDatabase.BoundingBox box in o.BoundingBoxes) {
                CreateChild(box.Centre, box.Size, box.Orientation);
            }
        }
        
        private void Update() {
            // prevent accidental scaling
            if (transform.localScale != Vector3.one) {
                transform.localScale = Vector3.one;
            }

            // prevent accidental rotation
            if (transform.eulerAngles != Vector3.zero) {
                transform.eulerAngles = Vector3.zero;
            }

            if (_children.Count == 0) {
                return;
            }

            Vector4[] pointBoundVectors = new Vector4[100];
            Matrix4x4[] pointBoundMatrices = new Matrix4x4[100];

            for (int i = 0; i < _children.Count; i++) {
                pointBoundMatrices[i] = _children[i].transform.localToWorldMatrix.inverse;
                pointBoundVectors[i] = _children[i].Dimensions;
            }

            Shader.SetGlobalInt("_PointBoundVectorLength", _children.Count);
            Shader.SetGlobalVectorArray("_PointBoundVectors", pointBoundVectors);
            Shader.SetGlobalMatrixArray("_PointBoundMatrices", pointBoundMatrices);
        }

        public bool IntersectsBounds(Bounds bounds) {
            return _AABB.Intersects(bounds) && _children.Any(child => child.IntersectsBounds(bounds));
        }

        public GameObject CreateChild(Vector3 position, Vector3 size, Quaternion orientation) {
            GameObject child = PrefabUtility.InstantiatePrefab(childBoundsPrefab) as GameObject;

            if (child == null) {
                throw new SelectionException("Unable to create child bounds from prefab");
            }

            child.transform.parent = transform;
            child.transform.position = Vector3.zero;
            child.transform.localRotation = Quaternion.identity;
            child.transform.localScale = Vector3.one;
            SelectionBox sb = child.GetComponent<SelectionBox>();

            sb.Init(position, size, orientation);
            
            UpdateChildren();

            return child;
        }

        public void DeleteChild(GameObject child) {
            if (child == null) {
                throw new NullReferenceException("GameObject passed in is null.");
            }
            
            if (!child.transform.IsChildOf(transform)) {
                throw new SelectionException("The object is not a child.");
            }

            SelectionBox sb = child.GetComponent<SelectionBox>();

            if (sb == null) {
                throw new SelectionException("The object doesn't have a SelectionBox component.");
            }
            
            if (!_children.Contains(sb)) {
                throw new SelectionException("The object doesn't appear in my list, but is part of the hierarchy. This isn't right!");
            }

            _children.Remove(sb);
            
            Destroy(child);
            UpdateChildren();
        }

        private void UpdateChildren() {
            SelectionBox[] childrenArray = GetComponentsInChildren<SelectionBox>();
            _children = childrenArray.ToList();
            _children.Sort((a, b) => b.Volume.CompareTo(a.Volume));

            EncapsulateChildren();
        }

        private void EncapsulateChildren() {
            if (_children.Count == 0) {
                return;
            }

            _AABB = _children[0].AABB;

            foreach (SelectionBox child in _children) {
                _AABB.Encapsulate(child.AABB);
            }
        }

        private void OnDrawGizmos() {
            EncapsulateChildren();

            if (debug) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireCube(_children.Count == 0 ? transform.position : worldPosition, worldDimensions);
                Handles.Label(transform.position, "Wibble");
            }
        }
    }
}