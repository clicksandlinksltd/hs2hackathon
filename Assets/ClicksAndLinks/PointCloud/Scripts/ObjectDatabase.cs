﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace ClicksAndLinks.HPC {
    public class VectorConverter : JsonConverter {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            if (value is Vector2) {
                Vector2 v = (Vector2) value;

                float[] vv = {v.x, v.y};
                JArray a = new JArray(vv);
                a.WriteTo(writer);
            } else if (value is Vector3) {
                Vector3 v = (Vector3) value;

                float[] vv = {v.x, v.y, v.z};
                JArray a = new JArray(vv);
                a.WriteTo(writer);
            } else if (value is Vector4) {
                Vector4 v = (Vector4) value;

                float[] vv = {v.x, v.y, v.z, v.w};
                JArray a = new JArray(vv);
                a.WriteTo(writer);
            } else {
                throw new NotImplementedException();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer) {
            JArray a = JArray.Load(reader);
            float[] vv = a.Values<float>().ToArray();

            if (vv.Length == 2) {
                return new Vector2(vv[0], vv[1]);
            }
            
            if (vv.Length == 3) {
                return new Vector3(vv[0], vv[1], vv[2]);
            }
            
            if (vv.Length == 4) {
                return new Vector4(vv[0], vv[1], vv[2], vv[3]);
            }
            
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType) {
            return objectType == typeof(Vector2) ||  objectType == typeof(Vector3) || objectType == typeof(Vector4);
        }

        public override bool CanWrite {
            get { return true; }
        }

        public override bool CanRead {
            get { return true; }
        }
    }

    public class QuaternionConverter : CustomCreationConverter<Quaternion> {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            Quaternion v = (Quaternion) value;
            
            float[] vv = {v.x, v.y, v.z, v.w};
            JArray a = new JArray(vv);
            a.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer) {
            JArray a = JArray.Load(reader);
            float[] vv = a.Values<float>().ToArray();
            Quaternion target = new Quaternion(vv[0], vv[1], vv[2], vv[3]);

            return target;
        }

        public override Quaternion Create(Type objectType) {
            return Quaternion.identity;
        }

        public override bool CanConvert(Type objectType) {
            return objectType == typeof(Quaternion);
        }

        public override bool CanWrite {
            get { return true; }
        }

        public override bool CanRead {
            get { return true; }
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    [Serializable]
    public class ObjectDatabase : ScriptableObject {
        public struct BoundingBox {
            [JsonProperty("centre")] public Vector3 Centre;
            [JsonProperty("orientation")][JsonConverter(typeof(QuaternionConverter))] public Quaternion Orientation;
            [JsonProperty("size")] [JsonConverter(typeof(VectorConverter))] public Vector3 Size;
        }

        public struct PointObject {
            [JsonProperty("name")] public string Name;
            [JsonProperty("children")] public List<BoundingBox> BoundingBoxes;
        }

        [JsonProperty("objects")] private Dictionary<Guid, PointObject> _database;
        private PointObject _selectedObject;
        private PointBoundParent _selectionBox;

        public Dictionary<Guid, string> ObjectNames {
            get {
                return _database.ToDictionary(o => o.Key, o => o.Value.Name);
            }
        }

        public PointObject SelectObject(Guid id) {
            if (!_database.ContainsKey(id)) {
                throw new IndexOutOfRangeException("Unable to find object with the id '" + id + "' in the database.");
            }
            
            _selectedObject = _database[id];
            _selectionBox.hideFlags = HideFlags.None;
            _selectionBox.Init(_selectedObject);
            return _selectedObject;
        }

        public void Deselect() {
            if (_selectionBox == null) {
                throw new NullReferenceException("Selection box object has not been instantiated.");
            }
            
            _selectionBox.hideFlags = HideFlags.HideInHierarchy;
        }

        private void Awake() {
            if (_selectionBox == null) {
                UnityEngine.Object o = Resources.Load("ParentBounds");
                _selectionBox = ((GameObject)PrefabUtility.InstantiatePrefab(o)).GetComponent<PointBoundParent>();
                Deselect();
            }
        }

        public void Save(string fileName) {
            using (StreamWriter file = File.CreateText(fileName)) {
                string json = JsonConvert.SerializeObject(_database, Formatting.Indented,
                    new JsonSerializerSettings {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                
                file.Write(json);
            }
        }

        public void Load(string fileName) {
            using (StreamReader file = File.OpenText(fileName)) {
                _database = JsonConvert.DeserializeObject<Dictionary<Guid, PointObject>>(file.ReadToEnd());
            }
        }
    }
}