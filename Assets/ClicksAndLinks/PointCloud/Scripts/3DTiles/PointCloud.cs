﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

// TODO: Create a tile MonoBehaviour for dealing with culling and so on.

namespace ClicksAndLinks.ThreeDTiles {
    public interface ITile {
        void Deserialize(Stream stream);
    }

    [TileType(new[] {'p', 'n', 't', 's'})]
    public class PointCloud : ITile {
        [Serializable]
        private struct Header {
            public uint version;
            public uint byteLength;
            public uint featureTableJSONByteLength;
            public uint featureTableBinaryByteLength;
            public uint batchTableJSONByteLength;
            public uint batchTableBinaryByteLength;
        }

        private Header _header;
        private const int HEADER_SIZE = 28;

        private static float signNotZero(float value) {
            return value >= 0.0f ? 1.0f : -1.0f;
        }


        private static Vector3 OctDecode(byte u, byte v) {
            Vector3 vec = new Vector3();

            vec.x = u / 128.0f - 1.0f;
            vec.y = v / 128.0f - 1.0f;
            vec.z = 1.0f - (Mathf.Abs(vec.x) + Mathf.Abs(vec.y));

            if (vec.z >= 0.0f) {
                return vec.normalized;
            }

            float oldx = vec.x;

            vec.x = (1.0f - Mathf.Abs(vec.y)) * signNotZero(oldx);
            vec.y = (1.0f - Mathf.Abs(oldx)) * signNotZero(vec.y);

            return vec.normalized;
        }

        public void Deserialize(Stream stream) {
            _header = new Header();
            PointCloudFeatureTable pcft;
            MemoryStream featuresBufferStream;

            using (BinaryReader reader = new BinaryReader(stream)) {
                _header.version = reader.ReadUInt32();
                _header.byteLength = reader.ReadUInt32();
                _header.featureTableJSONByteLength = reader.ReadUInt32();
                _header.featureTableBinaryByteLength = reader.ReadUInt32();
                _header.batchTableJSONByteLength = reader.ReadUInt32();
                _header.batchTableBinaryByteLength = reader.ReadUInt32();

                if (_header.version != 1) {
                    throw new UnsupportedVersionException("Only Point Cloud tile version 1 is supported.  Version " +
                                                          _header.version + " is not.");
                }

                if (_header.featureTableJSONByteLength == 0) {
                    throw new InvalidTileHeaderException("Feature table must have a byte length greater than zero.");
                }

                string jsonFeatureTable =
                    Encoding.UTF8.GetString(reader.ReadBytes((int) _header.featureTableJSONByteLength));
                pcft = JsonConvert.DeserializeObject<PointCloudFeatureTable>(jsonFeatureTable);

                if (pcft.Position == null && pcft.PositionQuantized == null) {
                    throw new InvalidTileHeaderException(
                        "There must be one of POSITION or POSITION_QUANTIZED in the feature table.");
                }

                if (pcft.Position != null && pcft.PositionQuantized != null) {
                    throw new InvalidTileHeaderException(
                        "There can only be one of POSITION or POSITION_QUANTIZED in the feature table.");
                }

                // TODO: Break this up into groups.
                if (pcft.PointsLength > 65530) {
                    Debug.LogWarning("There are too many points to render in one go. Truncating.");
                    pcft.PointsLength = 65530;
                }
                var featuresOffset =
                    HEADER_SIZE + _header.featureTableJSONByteLength + _header.batchTableJSONByteLength;

                // Load the entire block and work on that.
                stream.Seek(featuresOffset, SeekOrigin.Begin);
                byte[] featuresBuffer = reader.ReadBytes((int) _header.featureTableBinaryByteLength);
                featuresBufferStream = new MemoryStream(featuresBuffer, true);

                // TODO: Read and parse batch table, if any.
                // TODO: Speed this up! 100 point sample takes 38ms to load.
            }

            Vector3[] verts = new Vector3[pcft.PointsLength];
            Color32[] colours = new Color32[pcft.PointsLength];
            int[] indices = new int[pcft.PointsLength];
            Vector2[] uvs = new Vector2[pcft.PointsLength];
            Mesh mesh = new Mesh();

            BinaryReader featureReader = new BinaryReader(featuresBufferStream);

            if (pcft.Position != null) {
                featuresBufferStream.Seek(pcft.Position.ByteOffset, SeekOrigin.Begin);

                for (int i = 0; i < pcft.PointsLength; ++i) {
                    verts[i] = new Vector3(featureReader.ReadSingle(), featureReader.ReadSingle(),
                        featureReader.ReadSingle());
                    indices[i] = i;
                }
            } else if (pcft.PositionQuantized != null) {
                featuresBufferStream.Seek(pcft.PositionQuantized.ByteOffset, SeekOrigin.Begin);

                for (int i = 0; i < pcft.PointsLength; ++i) {
                    float x = featureReader.ReadInt16() * pcft.QuantizedVolumeScale.x / 65535.0f +
                              pcft.QuantizedVolumeOffset.x;
                    float y = featureReader.ReadInt16() * pcft.QuantizedVolumeScale.y / 65535.0f +
                              pcft.QuantizedVolumeOffset.y;
                    float z = featureReader.ReadInt16() * pcft.QuantizedVolumeScale.z / 65535.0f +
                              pcft.QuantizedVolumeOffset.z;
                    verts[i] = new Vector3(x, y, z);
                    indices[i] = i;
                }
            }

            if (pcft.Rgba != null) {
                featuresBufferStream.Seek(pcft.Rgba.ByteOffset, SeekOrigin.Begin);
                for (int i = 0; i < pcft.PointsLength; ++i) {
                    colours[i] = new Color32(featureReader.ReadByte(), featureReader.ReadByte(),
                        featureReader.ReadByte(), featureReader.ReadByte());
                }
            } else if (pcft.Rgb != null) {
                featuresBufferStream.Seek(pcft.Rgb.ByteOffset, SeekOrigin.Begin);
                for (int i = 0; i < pcft.PointsLength; ++i) {
                    colours[i] = new Color32(featureReader.ReadByte(), featureReader.ReadByte(),
                        featureReader.ReadByte(), 255);
                }
            } else if (pcft.Rgb565 != null) {
                featuresBufferStream.Seek(pcft.Rgb565.ByteOffset, SeekOrigin.Begin);
                for (int i = 0; i < pcft.PointsLength; ++i) {
                    ushort colour = featureReader.ReadUInt16();
                    byte red = (byte) ((colour & 0xf800) >> 8);
                    byte green = (byte) ((colour & 0x07e0) >> 3);
                    byte blue = (byte) ((colour & 0x001f) << 3);
                    colours[i] = new Color32(red, green, blue, 255);
                }
            } else {
                for (int i = 0; i < pcft.PointsLength; ++i) {
                    colours[i] = pcft.ConstantRgba;
                }
            }

            if (pcft.Normal != null) {
                featuresBufferStream.Seek(pcft.Normal.ByteOffset, SeekOrigin.Begin);

                mesh.normals = new Vector3[pcft.PointsLength];

                for (int i = 0; i < pcft.PointsLength; ++i) {
                    mesh.normals[i] = new Vector3(featureReader.ReadSingle(), featureReader.ReadSingle(),
                        featureReader.ReadSingle());
                }
            } else if (pcft.NormalOct16p != null) {
                featuresBufferStream.Seek(pcft.NormalOct16p.ByteOffset, SeekOrigin.Begin);

                mesh.normals = new Vector3[pcft.PointsLength];

                for (int i = 0; i < pcft.PointsLength; ++i) {
                    mesh.normals[i] = OctDecode(featureReader.ReadByte(), featureReader.ReadByte());
                }
            }

            if (pcft.BatchId != null) {
                featuresBufferStream.Seek(pcft.BatchId.ByteOffset, SeekOrigin.Begin);

                switch (pcft.BatchId.TheComponentType) {
                    case ComponentType.UNSIGNED_BYTE:
                        for (int i = 0; i < pcft.PointsLength; ++i) {
                            uvs[i] = new Vector2(featureReader.ReadByte(), 0.0f);
                        }
                        break;
                    case ComponentType.UNSIGNED_SHORT:
                        for (int i = 0; i < pcft.PointsLength; ++i) {
                            uvs[i] = new Vector2(featureReader.ReadUInt16(), 0.0f);
                        }
                        break;
                    case ComponentType.UNSIGNED_INT:
                        for (int i = 0; i < pcft.PointsLength; ++i) {
                            uvs[i] = new Vector2(featureReader.ReadUInt32(), 0.0f);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            } else {
                for (int i = 0; i < pcft.PointsLength; ++i) {
                    uvs[i] = new Vector2(0.0f, 0.0f);
                }
            }

            // TODO: Add materials.
            GameObject go = new GameObject();
            MeshFilter mf = go.AddComponent<MeshFilter>();
            MeshRenderer mr = go.AddComponent<MeshRenderer>();

            mesh.vertices = verts;
            mesh.colors32 = colours;
            mesh.uv = uvs;
            mesh.SetIndices(indices, MeshTopology.Points, 0);

            mf.mesh = mesh;
        }
    }
}