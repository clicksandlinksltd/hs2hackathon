﻿using System;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace ClicksAndLinks.ThreeDTiles {

	public class TilesetTest : MonoBehaviour {
		public string fileName;
		public Tileset tileset;

		private void Start() {
			Stopwatch sw = new Stopwatch();
			sw.Start();
			using (StreamReader file = File.OpenText(fileName)) {
				tileset = JsonConvert.DeserializeObject<Tileset>(file.ReadToEnd());
				tileset.RootUri = new Uri(Path.GetFullPath(fileName));
			}

			long split = sw.ElapsedMilliseconds;
			tileset.LoadTile(tileset.Root);
			UnityEngine.Debug.Log(split);
			UnityEngine.Debug.Log(sw.ElapsedMilliseconds - split);

//			using (StreamWriter file = File.CreateText("wibble_" + fileName)) {
//				string json = JsonConvert.SerializeObject(tileset, Formatting.Indented);
//
//				file.Write(json);
//			}
		}
	}
}
