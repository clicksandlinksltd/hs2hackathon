﻿using System.ComponentModel;
using ClicksAndLinks.HPC;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEngine;

namespace ClicksAndLinks.ThreeDTiles {
    public enum ComponentType {
        UNSIGNED_BYTE,
        UNSIGNED_SHORT,
        UNSIGNED_INT
    }

    [JsonObject]
    public class BinaryBodyReference {
        [JsonProperty("byteOffset", Required = Required.Always)] public int ByteOffset;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("componentType", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(ComponentType.UNSIGNED_SHORT)]
        public ComponentType TheComponentType;
    }

    [JsonObject]
    public class PointCloudFeatureTable {
        [JsonProperty("POINTS_LENGTH", Required = Required.Always)] public int PointsLength;

        [JsonProperty("POSITION", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public BinaryBodyReference Position;

        [JsonProperty("POSITION_QUANTIZED", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public BinaryBodyReference PositionQuantized;

        [JsonProperty("RGBA", DefaultValueHandling = DefaultValueHandling.Ignore)] public BinaryBodyReference Rgba;
        [JsonProperty("RGB", DefaultValueHandling = DefaultValueHandling.Ignore)] public BinaryBodyReference Rgb;
        [JsonProperty("RGB565", DefaultValueHandling = DefaultValueHandling.Ignore)] public BinaryBodyReference Rgb565;
        [JsonProperty("NORMAL", DefaultValueHandling = DefaultValueHandling.Ignore)] public BinaryBodyReference Normal;

        [JsonProperty("NORMAL_OCT16P", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public BinaryBodyReference NormalOct16p;

        [JsonProperty("BATCH_ID", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public BinaryBodyReference BatchId;

        [JsonProperty("RTC_CENTER", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [JsonConverter(typeof(VectorConverter))] public Vector3 RtcCenter;

        [JsonProperty("QUANTIZED_VOLUME_OFFSET", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [JsonConverter(typeof(VectorConverter))] public Vector3 QuantizedVolumeOffset;

        [JsonProperty("QUANTIZED_VOLUME_SCALE", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [JsonConverter(typeof(VectorConverter))] public Vector3 QuantizedVolumeScale;

        [JsonProperty("CONSTANT_RGBA", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [JsonConverter(typeof(ColorConverter), 4)] public Color32 ConstantRgba;

        [JsonProperty("BATCH_LENGTH", DefaultValueHandling = DefaultValueHandling.Ignore)] public int BatchLength;
    }
}