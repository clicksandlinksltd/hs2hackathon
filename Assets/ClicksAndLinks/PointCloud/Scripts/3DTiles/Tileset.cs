﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using UnityEngine;

// See: https://github.com/AnalyticalGraphicsInc/3d-tiles
// Based on the schemata here: https://github.com/AnalyticalGraphicsInc/3d-tiles/tree/master/schema
namespace ClicksAndLinks.ThreeDTiles {
    public enum Refine {
        ADD,
        REPLACE,

        INHERIT
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class TileTypeAttribute : Attribute {
        private byte[] _magic;

        public byte[] Magic {
            get { return _magic; }
            set { _magic = value; }
        }

        public TileTypeAttribute(char[] magic) {
            _magic = Encoding.ASCII.GetBytes(magic);
        }
    }

    public class UnsupportedTileTypeException : ApplicationException {
        public UnsupportedTileTypeException() {
        }

        public UnsupportedTileTypeException(string message)
            : base(message) {
        }

        public UnsupportedTileTypeException(string message, Exception inner)
            : base(message, inner) {
        }
    }

    public class UnsupportedVersionException : ApplicationException {
        public UnsupportedVersionException() {
        }

        public UnsupportedVersionException(string message)
            : base(message) {
        }

        public UnsupportedVersionException(string message, Exception inner)
            : base(message, inner) {
        }
    }

    public class InvalidTileHeaderException : ApplicationException {
        public InvalidTileHeaderException() {
        }

        public InvalidTileHeaderException(string message)
            : base(message) {
        }

        public InvalidTileHeaderException(string message, Exception inner)
            : base(message, inner) {
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public interface IBoundingVolume {
        double[] Values { get; }
    }

    /// <summary>
    /// An array of 12 numbers that define an oriented bounding box.  The first three elements define the x, y, and z
    /// values for the center of the box.  The next three elements (with indices 3, 4, and 5) define the x axis
    /// direction and half-length.  The next three elements (indices 6, 7, and 8) define the y axis direction and
    /// half-length.  The last three elements (indices 9, 10, and 11) define the z axis direction and half-length.
    /// </summary>
    public class Box : IBoundingVolume {
        public Bounds Bounds {
            get {
                if (_values[4] != 0.0 || _values[5] != 0.0 || _values[6] != 0.0 || _values[8] != 0.0 ||
                    _values[9] != 0.0 || _values[10] != 0.0) {
                    Debug.LogWarning("Only axis aligned bounding boxes are supported at this time.");
                }

                return new Bounds(new Vector3((float) _values[0], (float) _values[1], (float) _values[2]),
                    new Vector3((float) _values[3], (float) _values[7], (float) _values[11]));
            }
        }

        private readonly double[] _values;

        public Box(double[] values) {
            _values = new double[12];
            Array.Copy(values, _values, 12);
        }

        [JsonProperty("box")]
        public double[] Values {
            get { return _values; }
        }
    }

    /// <summary>
    /// An array of six numbers that define a bounding geographic region in WGS84 / EPSG:4326 coordinates with the
    /// order [west, south, east, north, minimum height, maximum height]. Longitudes and latitudes are in radians,
    /// and heights are in meters above (or below) the WGS84 ellipsoid.
    /// </summary>
    public class Region : IBoundingVolume {
        private readonly double _west;
        private readonly double _south;
        private readonly double _east;
        private readonly double _north;
        private readonly double _min;
        private readonly double _max;

        [JsonProperty("region")]
        public double[] Values {
            get { return new[] {_west, _south, _east, _north, _min, _max}; }
        }

        public Region(double west, double south, double east, double north, double minimumHeight,
            double maximumHeight) {
            _west = west;
            _south = south;
            _east = east;
            _north = north;
            _min = minimumHeight;
            _max = maximumHeight;
        }
    }

    /// <summary>
    /// An array of four numbers that define a bounding sphere.  The first three elements define the x, y, and z values
    /// for the center of the sphere.  The last element (with index 3) defines the radius in meters.
    /// </summary>
    public class Sphere : IBoundingVolume {
        public BoundingSphere Bounds {
            get { return new BoundingSphere(new Vector3((float) _x, (float) _y, (float) _z), (float) _radius); }
        }

        private readonly double _x;
        private readonly double _y;
        private readonly double _z;
        private readonly double _radius;

        public Sphere(double x, double y, double z, double radius) {
            _x = x;
            _y = y;
            _z = z;
            _radius = radius;
        }

        [JsonProperty("sphere")]
        public double[] Values {
            get { return new[] {_x, _y, _z, _radius}; }
        }
    }

    public class BoundingVolumeConverter : JsonConverter {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            IBoundingVolume r = (IBoundingVolume) value;
            JObject o = JObject.FromObject(r);
            o.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer) {
            JObject jObject = JObject.Load(reader);

            JToken value;

            if (jObject.TryGetValue("box", out value)) {
                double[] arr = value.ToObject<double[]>();
                if (arr.Length != 12) {
                    throw new IndexOutOfRangeException("Incorrect number of values for a \"box\". Expected 12, got " +
                                                       arr.Length);
                }

                return new Box(arr);
            }

            if (jObject.TryGetValue("region", out value)) {
                double[] arr = value.ToObject<double[]>();
                if (arr.Length != 6) {
                    throw new IndexOutOfRangeException("Incorrect number of values for a \"region\". Expected 6, got " +
                                                       arr.Length);
                }

                return new Region(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
            }

            if (jObject.TryGetValue("sphere", out value)) {
                double[] arr = value.ToObject<double[]>();
                if (arr.Length != 4) {
                    throw new IndexOutOfRangeException("Incorrect number of values for a \"sphere\". Expected 4, got " +
                                                       arr.Length);
                }

                return new Sphere(arr[0], arr[1], arr[2], arr[3]);
            }

            return null;
        }

        public override bool CanConvert(Type objectType) {
            return objectType == typeof(IBoundingVolume);
        }

        public override bool CanWrite {
            get { return true; }
        }
    }

    public class MatrixConverter : CustomCreationConverter<Matrix4x4> {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            Matrix4x4 m = (Matrix4x4) value;

            double[] vv = new double[16];

            for (int i = 0; i < 16; ++i) {
                vv[i] = m[i];
            }

            JArray a = new JArray(vv);
            a.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer) {
            JArray arr = JArray.Load(reader);

            if (arr.Count != 16) {
                Debug.LogWarning("Wrong number of components for a Matrix4x4. Expected 16, got " + arr.Count);
                return Matrix4x4.identity;
            }

            double[] vv = arr.Values<double>().ToArray();

            Matrix4x4 m = new Matrix4x4();

            for (int i = 0; i < 16; ++i) {
                m[i] = (float) vv[i];
            }

            return m;
        }

        public override Matrix4x4 Create(Type objectType) {
            return Matrix4x4.identity;
        }

        public override bool CanConvert(Type objectType) {
            return objectType == typeof(Matrix4x4);
        }

        public override bool CanWrite {
            get { return true; }
        }
    }

    public class ColorConverter : JsonConverter {
        private readonly int _numChannels;

        public ColorConverter(int numChannels = 4) {
            if (numChannels < 2 || numChannels > 4) {
                throw new IndexOutOfRangeException("Expected 2, 3 or 4 channels, got " + numChannels);
            }
            
            _numChannels = numChannels;
        }
        
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            byte[] vv = null;
            
            if (value is Color) {
                Color v = (Color) value;

                if (_numChannels == 3) {
                    vv = new[]{
                        (byte) Mathf.Max(v.r * 255.0f, 255.0f),
                        (byte) Mathf.Max(v.g * 255.0f, 255.0f),
                        (byte) Mathf.Max(v.b * 255.0f, 255.0f)
                    };
                } else if (_numChannels == 4) {
                    vv = new[]{
                        (byte) Mathf.Max(v.r * 255.0f, 255.0f),
                        (byte) Mathf.Max(v.g * 255.0f, 255.0f),
                        (byte) Mathf.Max(v.b * 255.0f, 255.0f),
                        (byte) Mathf.Max(v.a * 255.0f, 255.0f)
                    };
                }
            } else if (value is Color32) {
                Color32 v = (Color32) value;

                if (_numChannels == 3) {
                    vv = new[]{v.r, v.g, v.b};
                } else if (_numChannels == 4) {
                    vv = new[]{v.r, v.g, v.b, v.a};
                }
            } else {
                throw new NotImplementedException();
            }

            if (vv != null) {
                JArray a = new JArray(vv);
                a.WriteTo(writer);
            } else {
                throw new IndexOutOfRangeException();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer) {
            JArray a = JArray.Load(reader);
            byte[] vv = a.Values<byte>().ToArray();

            if (vv.Length == 3) {
                return new Color32(vv[0], vv[1], vv[2], 255);
            }
            
            if (vv.Length == 4) {
                return new Color32(vv[0], vv[1], vv[2], vv[3]);
            }
            
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType) {
            return objectType == typeof(Color) ||  objectType == typeof(Color32);
        }

        public override bool CanWrite {
            get { return true; }
        }

        public override bool CanRead {
            get { return true; }
        }
    }

    /// <summary>
    /// Metadata about the entire tileset.
    /// </summary>
    [JsonObject]
    public class Asset {

        /// <summary>
        /// Application-specific version of this tileset, e.g., for when an existing tileset is updated.
        /// </summary>
        [JsonProperty("tilesetVersion", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string TilesetVersion;

        /// <summary>
        /// Specifies the up-axis of glTF models.
        /// </summary>
        [JsonProperty("gltfUpAxis", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)] [DefaultValue("Y")]
        public string GltfUpAxisAxis;

        /// <summary>
        /// The 3D Tiles version.  The version defines the JSON schema for tileset.json and the base set of tile
        /// formats.
        /// </summary>
        [JsonProperty("version", Required = Required.Always)] [DefaultValue("1.0")] 
        public string Version {
            get { return _version; }
            set {
                if (value != "1.0") {
                    throw new UnsupportedVersionException("Expected version '1.0' got '" + value + "'");
                }
                _version = value;
            }
        }
        private string _version;
    }

    /// <summary>
    /// A dictionary object of metadata about per-feature properties.
    /// </summary>
    [JsonObject]
    public class Property {
        /// <summary>
        /// The maximum value of this property of all the features in the tileset.
        /// </summary>
        [JsonProperty("minimum", Required = Required.Always)] public double Minimum;

        /// <summary>
        /// The minimum value of this property of all the features in the tileset.
        /// </summary>
        [JsonProperty("maximum", Required = Required.Always)] public double Maximum;
    }

    /// <summary>
    /// A tile in a 3D Tiles tileset.
    /// </summary>
    [JsonObject]
    public class Tile {
        /// <summary>
        /// The bounding volume that encloses the tile.
        /// </summary>
        [JsonConverter(typeof(BoundingVolumeConverter))] [JsonProperty("boundingVolume", Required = Required.Always)]
        public IBoundingVolume BoundingVolume;

        /// <summary>
        /// Optional bounding volume that defines the volume that the viewer must be inside of before the tile's
        /// content will be requested and before the tile will be refined based on geometricError.
        /// </summary>
        [JsonConverter(typeof(BoundingVolumeConverter))]
        [JsonProperty("viewerRequestVolume", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public IBoundingVolume ViewerRequestVolume;

        /// <summary>
        /// The error, in meters, introduced if this tile is rendered and its children are not. At runtime, the
        /// geometric error is used to compute Screen-Space Error (SSE), i.e., the error measured in pixels.
        /// </summary>
        [JsonProperty("geometricError", Required = Required.Always)] public double GeometricError;

        /// <summary>
        /// Specifies if additive or replacement refinement is used when traversing the tileset for rendering.
        /// This property is required for the root tile of a tileset; it is optional for all other tiles.
        /// The default is to inherit from the parent tile.
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("refine", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [DefaultValue(Refine.INHERIT)] public Refine Refine;

        /// <summary>
        /// A floating-point 4x4 affine transformation matrix, stored in column-major order, that transforms the tile's
        /// content, i.e., its features and content.boundingVolume, and boundingVolume and viewerRequestVolume from the
        /// tile's local coordinate system to the parent tile's coordinate system, or tileset's coordinate system in
        /// the case of the root tile.  transform does not apply to geometricError nor does it apply any volume
        /// property when the volume is a region, which is defined in WGS84 / EPSG:4326 coordinates.
        /// </summary>
        [JsonConverter(typeof(MatrixConverter))]
        [JsonProperty("transform", DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Matrix4x4 Transform = Matrix4x4.identity;

        /// <summary>
        /// Metadata about the tile's content and a link to the content. When this is omitted the tile is just used for
        /// culling. This is required for leaf tiles.
        /// </summary>
        [JsonProperty("content", DefaultValueHandling = DefaultValueHandling.Ignore)] public Content Content;

        /// <summary>
        /// An array of objects that define child tiles. Each child tile has a box fully enclosed by its parent tile's
        /// box and, generally, a geometricError less than its parent tile's geometricError. For leaf tiles, the length
        /// of this array is zero, and children may not be defined.
        /// </summary>
        [JsonProperty("children", DefaultValueHandling = DefaultValueHandling.Ignore)] public List<Tile> Children;
    }

    /// <summary>
    /// Metadata about the tile's content and a link to the content.
    /// </summary>
    [JsonObject]
    public class Content {
        /// <summary>
        /// An optional bounding volume that tightly encloses just the tile's contents. This is used for replacement
        /// refinement; tile.boundingVolume provides spatial coherence and tile.content.boundingVolume enables tight
        /// view frustum culling. When this is omitted, tile.boundingVolume is used.
        /// </summary>
        [JsonConverter(typeof(BoundingVolumeConverter))]
        [JsonProperty("boundingVolume", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public IBoundingVolume BoundingVolume;

        /// <summary>
        /// A string that points to the tile's contents with an absolute or relative url. When the url is relative, it
        /// is relative to the referring tileset.json. The file extension of content.url defines the tile format. The
        /// core 3D Tiles spec supports the following tile formats: Batched 3D Model (*.b3dm), Instanced 3D Model
        /// (*.i3dm), Composite (*.cmpt), and 3D Tiles TileSet (*.json)
        /// </summary>
        [JsonProperty("url", Required = Required.Always)] public Uri Url;
    }

    /// <summary>
    /// A 3D Tiles tileset.
    /// </summary>
    [JsonObject]
    public class Tileset {
        [JsonProperty("asset", Required = Required.Always)] public Asset Asset;

        /// <summary>
        /// A dictionary object of metadata about per-feature properties.
        /// </summary>
        [JsonProperty("properties", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Dictionary<string, Property> Properties;

        /// <summary>
        /// The error, in meters, introduced if this tileset is not rendered. At runtime, the geometric error is used
        /// to compute Screen-Space Error (SSE), i.e., the error measured in pixels.
        /// </summary>
        [JsonProperty("geometricError", Required = Required.Always)] public double GeometricError;

        /// <summary>
        /// The root node.
        /// </summary>
        [JsonProperty("root", Required = Required.Always)] public Tile Root;

        private Uri _rootUri;
        private List<Type> _tileTypes;

        public Uri RootUri {
            get { return _rootUri; }
            set { _rootUri = value; }
        }

        public Tileset() {
            _tileTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => Attribute.IsDefined(t, typeof(TileTypeAttribute))).ToList();
        }

        public void LoadTile(Tile tile) {
            // TODO: Handle geospatial coordinates and so on.
            // TODO: Handle children that are tileset.json files.
            // TODO: Refactor exceptions.
            // TODO: Refactor converters.

            Uri contentUri = new Uri(_rootUri, tile.Content.Url);
            Debug.Log(contentUri);

            // TODO: This asynchronously.
            var webClient = new WebClient();

            Stream stream = webClient.OpenRead(contentUri);

            if (stream == null) {
                return;
            }
            

            byte[] magic = new byte[4];

            stream.Read(magic, 0, 4);

            Type tt = _tileTypes.FirstOrDefault(t =>
                ((TileTypeAttribute) Attribute.GetCustomAttribute(t, typeof(TileTypeAttribute))).Magic
                .SequenceEqual(magic));

            if (tt == null) {
                throw new UnsupportedTileTypeException();
            }

            ITile ii = (ITile) Activator.CreateInstance(tt);
            ii.Deserialize(stream);
        }
    }
}