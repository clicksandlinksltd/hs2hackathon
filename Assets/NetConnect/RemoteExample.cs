﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

[Serializable]
public class Position {
	// position of display object
	public float x;
	public float y;
	public float z;
	// rotation of display object
	public float xr;
	public float yr;
	public float zr;

	public void ReadData(GameObject display) {
		if (display != null) {
			Vector3 pos = display.transform.position;
			x = pos.x;
			y = pos.y;
			z = pos.z;
			Vector3 rot = display.transform.rotation.eulerAngles;
			xr = rot.x;
			yr = rot.y;
			zr = rot.z;
		}
	}

	public string ToString() {
		Vector3 pos = new Vector3(x,y,z);
		Vector3 rotE = new Vector3 (xr, yr, zr);
		string data = "Position " + pos.ToString () + " Rotation " + rotE.ToString ();
		return data;
	}
}


[RequireComponent(typeof(RemoteConnection))]
public class RemoteExample : MonoBehaviour {

	public Text serverDisplay;
	public Text dataInOutText; 
	public float sendInterval = 0.2f;

	bool sendingData = false;

	public GameObject display;

	[Header("DSG pose")]
	public Position receivedData;

	RemoteConnection connection;

	bool newData = false;
	
	void Start () {
		newData = false;
		
		connection = GetComponent<RemoteConnection> ();
		connection.AddMessageCallback ("examplemessage", HandleExampleMessage);
	}

	// Update is called once per frame
	void Update () {
		if (serverDisplay != null) {
			if (connection.remoteHosts.Count > 0) {
				serverDisplay.text = "Connected remotes : ";
				if(!connection.server)
				SendExampleMessage ();
			} else {
				serverDisplay.text = "No connected remotes";
			}
			foreach (string remote in connection.remoteHosts) {
				serverDisplay.text += remote + ", ";
			}
		}

		// start sending position courutine
		if (!connection.server && !sendingData) {
			sendingData = true;
			StartCoroutine (SendDataPeriodically());
		}

		// React to new data
		if (newData) {
			if (dataInOutText != null) {
				dataInOutText.text = "Received " + receivedData.ToString();
			}
			newData = false;
		}
	}

	IEnumerator SendDataPeriodically() {
		while (sendingData) {
			SendExampleMessage ();
			yield return new WaitForSeconds (sendInterval);
		}
		yield break;
	}

	public void SendExampleMessage()
	{
		if (connection.remoteHosts.Count == 0) {
			Debug.Log("RemoteMap - No remote hosts connected");
			return;
		}
		Position position = new Position();
		position.ReadData (display);
		string dataText = JsonUtility.ToJson (position);
		byte[] exampleData = System.Text.Encoding.ASCII.GetBytes(dataText);
		foreach (string remoteHost in connection.remoteHosts) {
			connection.SendRemoteMessage (remoteHost, "examplemessage", exampleData);
			dataInOutText.text = "Sent " + dataText;
			Debug.Log("RemoteMap - Sent examplemessage to " + remoteHost);
		}
	}

	public void HandleExampleMessage (byte[] data)
	{
		if (!newData) {
			string receivedMessage = System.Text.Encoding.UTF8.GetString(data);
			receivedData = JsonUtility.FromJson<Position> (receivedMessage);
			newData = true;
		}
	}

}
