﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections.Generic;
using System;
using System.Text;


public delegate void MessageHandlerDel (byte[] data);

[RequireComponent (typeof(NetworkDiscovery))]
[RequireComponent (typeof(RemoteMessageFragmenter))]
public class RemoteConnection : MonoBehaviour
{

	public bool wifiDiscoveryStarted = false;
	public bool server = false;
	public int commsPort = 47865;
	public List<string> remoteHosts = new List<string> ();
	public int maxTransferUnit = 2048;

	IPEndPoint receiverEndPoint = null;
	UdpClient receiver = null;
	//		TcpClient receiver = null;
	float timerSecs = 0f;
	string msgSeparator = RemoteMessageFragmenter.msgSeparator;
	bool closing = false;
	RemoteMessageFragmenter messageFragments;

	const string SHUTDOWNMSG = "clientshutdown";
	const string PINGMSG = "pingfromclient";

	Dictionary<string, MessageHandlerDel> messageCallbacks = new Dictionary<string, MessageHandlerDel> ();

	public bool localHost = true;

	// if remote sends shutdown message, then discovery part works for some time, so just wait some time to dont add it once again
	bool remoteRemoved = false;

	NetworkDiscovery disco;
	// Use this for initialization
	void Start ()
	{
		//listen for messages whether server or client
		if (localHost) {
			receiverEndPoint = new IPEndPoint (IPAddress.Loopback, commsPort);
		} else {
			receiverEndPoint = new IPEndPoint (IPAddress.Any, commsPort);
		}
		receiver = new UdpClient (receiverEndPoint);
		receiver.DontFragment = false;
		receiver.BeginReceive (RecieveCallback, null);
		messageFragments = GetComponent<RemoteMessageFragmenter> ();
		if (messageFragments == null) {
			Debug.LogError ("Message fragments not found!");
		}
		Debug.Log ("listening " + receiver.Client.LocalEndPoint.ToString ());
	}

	int updatesCounter = 0;

	// Update is called once per frame
	void Update ()
	{
//			++updatesCounter;
		if (remoteRemoved) {
			timerSecs = 0f;
			remoteRemoved = false;
		}
		timerSecs += Time.deltaTime;
		if (timerSecs > 2f) {
			if (!remoteRemoved) {
				DoDiscovery ();
			}
			timerSecs = 0f;
		}
	}

	void OnDestroy ()
	{
		Debug.LogWarning ("RemoteConnection shutting down");
		foreach (string host in remoteHosts) {
			SendRemoteMessage (host, SHUTDOWNMSG);	
		}

		NetworkDiscovery disco = GetComponent<NetworkDiscovery> ();		
		//ping server to add me
		if (disco.running) {
			disco.StopBroadcast ();
		}
		closing = true;
		receiver.Close ();

	}

	void DoDiscovery ()
	{

		if (!wifiDiscoveryStarted) {
			NetworkDiscovery discovery = GetComponent<NetworkDiscovery> ();
			discovery.Initialize ();
			if (server) {
				wifiDiscoveryStarted = discovery.StartAsServer ();
				Debug.Log ("RemoteConnection disco StartAsServer returned " + wifiDiscoveryStarted);
			} else {
				wifiDiscoveryStarted = discovery.StartAsClient ();
				Debug.Log ("RemoteConnection disco StartAsClient returned " + wifiDiscoveryStarted);
				
			}

		} else {
			if (!server) {
				//get server adverts
				NetworkDiscovery discovery = GetComponent<NetworkDiscovery> ();
				Dictionary<string, NetworkBroadcastResult> broadcasts = discovery.broadcastsReceived;
				// or maybe here just see what is in broadcasts, and then create
				// list of remotes that are here anymore...
				foreach (string host in broadcasts.Keys) {
					if (!remoteHosts.Contains (host)) {
						remoteHosts.Add (host);
						Debug.Log ("RemoteConnection found server : " + host);
					}

					//ping server to add me
					SendRemoteMessage (host, PINGMSG);
				}  
			}
		}
	}


	void RecieveCallback (IAsyncResult oASyncRes)
	{
		if (closing) {
			Debug.LogWarning ("RemoteConnection closing receiver");
			return;
		}

		byte[] oBytes = receiver.EndReceive (oASyncRes, ref receiverEndPoint);
		receiver.BeginReceive (RecieveCallback, null);

		string remoteSender = receiverEndPoint.Address.ToString ();

		if (server && !remoteHosts.Contains (remoteSender)) {
			//keep a list of clients sending to me
			Debug.Log ("RemoteConnection message from new client : " + remoteSender);
			remoteHosts.Add (remoteSender);
		}
			
		string receivedMessage = Encoding.ASCII.GetString (oBytes, 0, oBytes.Length);
		if (receivedMessage.StartsWith (SHUTDOWNMSG + msgSeparator)) {
			remoteRemoved = true;
			string prefixedRemoteSender = "::ffff:" + remoteSender;	// ipv6 compatibility address, all because of eaten fruit
			remoteHosts.Remove (remoteSender);
			remoteHosts.Remove (prefixedRemoteSender);


			Debug.Log ("RemoteConnection shutdown message from client : " + remoteSender);
		}

		foreach (string message in messageCallbacks.Keys) {
			string messageName = message + msgSeparator;

			if (messageFragments.IsSplitMessageName (receivedMessage, message)) {
				//	Debug.Log ("RemoteConnection recieved registered message fragment : " + message);

				int messageIdent = messageFragments.ProcessMessageFragment (receivedMessage, oBytes);
				if (messageFragments.IsFinishedSplitMessage (messageIdent)) {
					byte[] messageBuf = messageFragments.GetFinishedMessage (messageIdent);

					//run the callback registered on that message type
					//	Debug.Log ("RemoteConnection recieved registered split message : " + message);
						
					messageCallbacks [message] (messageBuf);
				}

			} else if (receivedMessage.StartsWith (messageName)) {

				try {
					byte[] messageBuf = new byte[oBytes.Length - messageName.Length];
					Buffer.BlockCopy (oBytes, messageName.Length, messageBuf, 0, messageBuf.Length);

					//run the callback registered on that message type
//					Debug.Log ("RemoteConnection recieved registered message : " + message);
//					Debug.Log(oBytes.Length + " " + messageName.Length);

					messageCallbacks [message] (messageBuf);
				} catch (Exception ex) {
					Debug.LogError ("RemoteConnection expn handling : " + ex.Message + ";" + ex.StackTrace);

				}
			}
		}


	}

	public void AddMessageCallback (string messageName, MessageHandlerDel callback)
	{
		Debug.Log("Adding message:" + messageName);
		messageCallbacks.Add (messageName, callback);
	}

	public void SendRemoteMessage (string remoteHost, string messageName)
	{
		SendRemoteMessage (remoteHost, messageName, new byte[]{ });
	}

	public void SendRemoteMessage (string remoteHost, string messageName, byte[] data)
	{
		//if message is bigger than a certain size
		if (data.Length > maxTransferUnit) {

			//then need to fragment message - IPV6 forces us to do this ourselves
			int fragmentCount = (int)Math.Ceiling ((double)data.Length / (double)maxTransferUnit);
//				int messageIdent = UnityEngine.Time.frameCount;
			int messageIdent = updatesCounter++;

			for (int fragmentIndex = 0; fragmentIndex < fragmentCount; fragmentIndex++) {			
				int count = maxTransferUnit;
				if (fragmentIndex == fragmentCount - 1) {
					count = data.Length - fragmentIndex * maxTransferUnit;
				}
				byte[] dataFragment = new byte[count];
				Buffer.BlockCopy (data, fragmentIndex * maxTransferUnit, dataFragment, 0, count);

				//use splitter object
				string messageFragmentName = messageFragments.GetMessageFragmentName (messageName, fragmentIndex, fragmentCount, messageIdent);

				SendRemoteMessageFragment (remoteHost, messageFragmentName, dataFragment);

			}
			//	Debug.Log ("RemoteConnection sent fragmented message : " + messageName + " " + data.Length);


		} else {
			//otherwise just send the message
			SendRemoteMessageFragment (remoteHost, messageName, data);
			Debug.Log ("RemoteConnection sent message : " + messageName + " " + data.Length);

		}

	}

	private void SendRemoteMessageFragment (string remoteHost, string messageName, byte[] data)
	{
		int ip6Index = remoteHost.LastIndexOf (":");
		if (ip6Index > 0) {
			//this code handles ipv6 hosts over ipv4 - was getting android exceptions without it
			remoteHost = remoteHost.Substring (ip6Index + 1);
		}
		IPEndPoint endPoint = new IPEndPoint (IPAddress.Parse (remoteHost), commsPort);
			
		byte[] messageNameBuf = Encoding.ASCII.GetBytes (messageName + msgSeparator);
		byte[] messageBuf = new byte[messageNameBuf.Length + data.Length];
		messageNameBuf.CopyTo (messageBuf, 0);
		data.CopyTo (messageBuf, messageNameBuf.Length);

		try {
			UdpClient sender = new UdpClient ();
			sender.DontFragment = false;
			int ret = sender.Send (messageBuf, messageBuf.Length, endPoint);
			sender.Close ();
			//	Debug.Log ("RemoteConnection sent message : " + messageName + " " + ret);

		} catch (SocketException ex) {

			Debug.LogError ("RemoteConnection socket expn : " + ex.SocketErrorCode + " - " + ex.Message + ";\n" + ex.StackTrace + ":");
		}
	}


}
