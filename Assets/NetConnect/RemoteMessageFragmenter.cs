﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public struct MessageFragment {
	
	public int messageIdent;
	public string messageName;
	public int fragmentIndex;
	public int fragmentCount;
	public byte[] data;
	
}

public class RemoteMessageFragmenter : MonoBehaviour {

	public const string msgSeparator = ":";
	public const string fragmentInfoSeparator = ";";
	public int fragmentCacheLength;

	public Dictionary<int, List<MessageFragment>> messageFragmentCache = new Dictionary<int, List<MessageFragment>>();

	void Start () {
	}

	void Update () {
	}

	public bool IsSplitMessageName (string receivedMessage, string messageName)
	{
		return receivedMessage.StartsWith (messageName + fragmentInfoSeparator);
	}

	public string GetMessageFragmentName (string messageName, int fragmentIndex, int fragmentCount, int messageIdent)
	{
		return messageName + fragmentInfoSeparator + messageIdent + fragmentInfoSeparator + fragmentIndex + fragmentInfoSeparator + fragmentCount;
	}

	public int ProcessMessageFragment (string receivedMessage, byte[] messageBuf)
	{
		int headerEnd = receivedMessage.IndexOf (msgSeparator);
		if (headerEnd < 0) {
			return -1;
		}
		string fragmentHeader = receivedMessage.Substring (0, headerEnd);
		string[] fragmentHeaders = fragmentHeader.Split (new string[]{fragmentInfoSeparator}, System.StringSplitOptions.RemoveEmptyEntries);

		MessageFragment fragment = new MessageFragment ();
		fragment.messageName = fragmentHeaders [0];
		if(!int.TryParse (fragmentHeaders [1], out fragment.messageIdent)){
			return -1;
		}
		if(!int.TryParse (fragmentHeaders [2], out fragment.fragmentIndex)){
			return -1;
		}
		if(!int.TryParse (fragmentHeaders [3], out fragment.fragmentCount)){
			return -1;
		}

		fragment.data = new byte[messageBuf.Length - (headerEnd + 1)];
		Buffer.BlockCopy(messageBuf, headerEnd + 1, fragment.data, 0, fragment.data.Length);

		if (!messageFragmentCache.ContainsKey(fragment.messageIdent)) {
			messageFragmentCache.Add (fragment.messageIdent, new List<MessageFragment>(fragment.fragmentCount));
		}

		messageFragmentCache [fragment.messageIdent].Add (fragment);
		fragmentCacheLength ++;
		return fragment.messageIdent;
	}

	public bool IsFinishedSplitMessage (int messageIdent)
	{
		return (messageFragmentCache [messageIdent] [0].fragmentCount == messageFragmentCache [messageIdent].Count);
	}

	public byte[] GetFinishedMessage (int messageIdent)
	{
		//need to sort the list by fragmentIndex
		messageFragmentCache [messageIdent].Sort ((frag1, frag2) => frag1.fragmentIndex.CompareTo(frag2.fragmentIndex));

		//and make a buffer for the complete message
		int totalByteCount = 0;
		foreach (MessageFragment fragment in messageFragmentCache [messageIdent]) {
			totalByteCount += fragment.data.Length;
		}
		byte[] completeMessage = new byte[totalByteCount];
		int byteIndex = 0;

		foreach (MessageFragment fragment in messageFragmentCache [messageIdent]) {
			Buffer.BlockCopy(fragment.data, 0, completeMessage, byteIndex, fragment.data.Length);
			byteIndex += fragment.data.Length;
		}
		fragmentCacheLength -= messageFragmentCache [messageIdent] [0].fragmentCount;
		messageFragmentCache.Remove (messageIdent);

		return completeMessage;
	}
}
