﻿using UnityEngine;

public class Map : MonoBehaviour {
    public GameObject Marker;

    public float MinEasting;
    public float MinNorthing;
    public float MaxEasting;
    public float MaxNorthing;

    private float _scaleX;
    private float _scaleY;

    private RectTransform _thisRt;
    private RectTransform _markerRt;

    private void Start() {
        _thisRt = GetComponent<RectTransform>();
        _markerRt = Marker.GetComponent<RectTransform>();
        _scaleX = (MaxEasting - MinEasting) / _thisRt.rect.width;
        _scaleY = -(MaxNorthing - MinNorthing) / _thisRt.rect.height;

        Tracker.OnPositionChanged += TrackerOnPositionChanged;
    }

    private void TrackerOnPositionChanged(float easting, float northing, float altitude) {
//        Vector3 pos = new Vector3((easting - MinEasting) / _scaleX, (northing - MinNorthing) / _scaleY, 0.0f);
        Vector3 pos = new Vector3((easting - MinEasting) / _scaleX, (northing - MinNorthing) / _scaleY, 0.0f);
        // 0 == -512
        // 100 == -256
        // 150 == -128
        // 200 == 0
//        pos.y = -512.0f + (pos.y);
        _markerRt.anchoredPosition = pos;
//        Debug.Log(northing + " " + pos.y);
    }
}