﻿using UnityEngine;

public class Tracker : MonoBehaviour {
	public GameObject Server;
	public GameObject Origin;
	public float Easting;
	public float Northing;
	public float Altitude;

	public delegate void PositionChanged(float easting, float northing, float altitude);
	public static event PositionChanged OnPositionChanged;
	
	private Origin _origin;
	private Position _headsetPosition;
	private RemoteExample _server;

	private void Start() {
		_origin = Origin.GetComponent<Origin>();

		_server = Server.GetComponent<RemoteExample>();
	}

	private void Update() {
		_headsetPosition = _server.receivedData;
		transform.position = new Vector3(_headsetPosition.x, _headsetPosition.y, _headsetPosition.z);
		Quaternion q = new Quaternion();
		q.eulerAngles = new Vector3(_headsetPosition.xr, _headsetPosition.yr, _headsetPosition.zr);
		transform.rotation = q;
		Vector3 offset = Origin.transform.TransformPoint(transform.position);
		Easting = _origin.Easting + offset.x;
		Northing = _origin.Northing + offset.z;
		Altitude = _origin.Altitude + offset.y;

		if (OnPositionChanged != null) {
			OnPositionChanged(Easting, Northing, Altitude);
		}
	}
}
