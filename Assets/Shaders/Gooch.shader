﻿Shader "ClicksAndLinks/Gooch" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_RimColor("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower("Rim Power", Range(0.5, 8.0)) = 3.0
		_SpecColor("Specular Color", Color) = (0.0, 0.0, 0.0, 1.0)
		_SpecPower("Specular Power", Range(0.5, 128.0)) = 3.0
        _Alpha ("Alpha", Float) = 0.2
        _Beta ("Beta", Float) = 0.6
        _b ("Blue", Float) = 0.4
        _y ("Yellow", Float) = 0.4
	}

	CGINCLUDE
		#include "UnityCG.cginc"

	ENDCG

	SubShader {
		Tags { "RenderType"="Opaque" "Queue" = "Geometry" }
		LOD 200
		// Push the geometry back a bit to make the lines stand out.
		Offset 1, -1
		
		CGPROGRAM
		#pragma surface surf NPR noambient
		#define USE_THE_RIM 1

		struct Input {
			float2 uv_MainTex;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};

		uniform sampler2D _MainTex;
		#if USE_THE_RIM
		uniform float4 _RimColor;
		uniform float _RimPower;
		#endif
		uniform float _SpecPower;

		uniform float _Alpha;
		uniform float _Beta;
		uniform float _b;
		uniform float _y;

		half4 LightingNPR(SurfaceOutput o, half3 lightdir, half3 viewdir, half atten)
		{
		    // Simulates a headlight on the camera. Normally you would use lightdir instead of viewdir.
			float lambert = (1.0 + (dot(o.Normal, viewdir))) / 2.0;
			
			half3 cool = (half3(0.0, 0.0, _b) + (o.Albedo * _Alpha)) * (1.0 - lambert);
			half3 warm = (half3(_y, _y, 0.0) + (o.Albedo * _Beta)) * lambert;
			half4 diffuse = half4(cool + warm, o.Alpha);

			half3 r = reflect(-lightdir, o.Normal);
			float phong = pow(saturate(dot(r, viewdir)), _SpecPower);
			half4 specular = half4(phong * _SpecColor * atten);

            #if USE_THE_RIM
			float rim_term = 1.0 - saturate(dot(viewdir, o.Normal));
			rim_term = pow(rim_term, _RimPower);
			half4 rim = half4(_RimColor.rgb * rim_term, 1.0);
			return diffuse - rim + specular;
			#else
			return diffuse + specular;
			#endif
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
			UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {
		    UNITY_SETUP_INSTANCE_ID(IN);
			fixed4 c = fixed4(1.0, 1.0, 1.0, 1.0);//tex2D(_MainTex, IN.uv_MainTex);	// Sample the texture
			o.Albedo = UNITY_ACCESS_INSTANCED_PROP(_Color).rgb * c.rgb;					// Modulate by main colour
			o.Alpha = 1.0;								// No alpha in this shader
		}
		ENDCG
	}
	FallBack "Diffuse"
}
